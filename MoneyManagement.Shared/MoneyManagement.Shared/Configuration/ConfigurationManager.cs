﻿using System;
using System.IO;

namespace MoneyManagement.Shared
{
    public class ConfigurationManager
    {

        public static string ApplicationBasePath
        {
            get
            {
#if __ANDROID__
                return Path.Combine(Android.OS.Environment.ExternalStorageDirectory.ToString(), "MoneyManagement");
#endif

#if __IOS__
                return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
#endif
            }
        }

        public static string DataPath => Path.Combine(ApplicationBasePath, "Data");
        public static string ProjectRootPath =>  Path.Combine(DataPath, "ProjectRoot");

        public static string GetConnectionString()
        {
            string connectionString = "Data Source = ";
           
           connectionString += Path.Combine(ApplicationBasePath, "Data","MoneyManagement.db3");
            return connectionString;
        }
    }
         
    }
}
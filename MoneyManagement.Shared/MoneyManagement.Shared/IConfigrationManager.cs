﻿using System;
namespace SKM_MOBILE.Core.Interfaces
{
    public interface IDeviceConfigrationManager
    {
        string GetApplicationBasePath();
        string GetAttachmentBasePath(string ProjectFolderName);
    }

}

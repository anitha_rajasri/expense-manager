﻿using System;
using System.Collections.Generic;
using MoneyManagement.Shared.Models;

namespace MoneyManagement.Shared.Helper
{

    public static class StoreProcedures
    {
        static string ProjectComponentID = String.Empty;
        public static Dictionary<string, string> StoreProcedure = new Dictionary<string, string>{
            {"GetApplicationDetails","SELECT PROPERTY,VALUE,DESCRIPTION FROM ApplicationProperties;"},
            {"GetProjectsList","select ProjectID,ProjectName,ProjectFolder,CreatedOn,LastImportedOn,LastExportedOn from Projects;"},
            {"GetProjectProperties","Select Property,Value,Description from ProjectProperties"},
            {"GetProjectComponentByID","select ProjectComponentID,ComponentID,ProjectComponentName from ProjectComponents where ProjectComponentID=@ProjectComponentID;"
                        + "select ProjectComponentPropertyID, ProjectComponentID, ComponentPropertyID, cast(isDataNull as bit) as isDataNull, Value, cast(isUnitNull as bit) as isUnitNull, UnitValue, cast(isLockedByProperty as bit) as isLockedByProperty, cast(isLockedByOtherProperty as bit) as isLockedByOtherProperty,"
                + "cast(isMadeInvisibleByOtherProperty as bit) as isMadeInvisibleByOtherProperty, cast(isLocked as bit) as isLocked from ProjectComponentProperties where ProjectComponentID =@ProjectComponentID"},
            {"GetProjectComponents","SELECT ProjectComponentID,ComponentID,ProjectComponentName FROM ProjectComponents order by lower(ProjectComponentName);"},
            //Images, Icon fields are removed by mg on : 20-01-2019
            {"GetComponents","SELECT ComponentID, ComponentName,  ComponentPrefix, MinVersion, MaxVersion, DataBlockID_BranchUI, DataBlockID_BrachSmallUI, DataBlockID_OnelineUI, DataBlockID_OnelineSmallUI, ComponentConnectionSpecialLogic  FROM Components order by ComponentName;"},
            {"GetProjectComponentPropertiesByID","select ProjectComponentPropertyID,ProjectcomponentID,ComponentPropertyID,cast(isDataNull as bit) as isDataNull,Value, "
                           + " cast(isUnitNull as bit) as isUnitNull,UnitValue,cast(isLockedByProperty as bit) as isLockedByProperty,cast(isLockedByOtherProperty as bit) as isLockedByOtherProperty,cast(isMadeInvisibleByOtherProperty as bit) as isMadeInvisibleByOtherProperty, "
                + "cast(isLocked as bit) as isLocked from  projectComponentProperties where ComponentPropertyID = @ComponentPropertyID  and ProjectComponentID=@ProjectComponentID;"},

            {"GetComponentByID","   SELECT c.*  "
                + " FROM Components c   "
                + " WHERE c.ComponentID = @ComponentID ;   "
                + "     "
                //+ " -- Table 1  "
                + "     "
                + " SELECT ComponentPropertyID, ComponentID, cast(isNameProperty as bit) as isNameProperty, cast(isSystemNominalVoltage as bit) as isSystemNominalVoltage, PropertyName, XMLColumnName, ShortName, Description, MinVersion, MaxVersion, DataType, DataSize, DisplayFormatString, DataStorageFormatString, DataTypeMetric, DataSizeMetric, DisplayFormatStringMetric, DataStorageFormatStringMetric, DataTypeImportExport, DataSizeImportExport, ExportFormatString, ExportFormatStringMetric, ConvertDataToImerialUnit, NullInImportExport, UIType, cast(isReadOnly as bit) as isReadOnly, cast(isEditableWhenNullOrZero as bit) as isEditableWhenNullOrZero, DisplayOrderInComponentEditor, LOVGroupID, LOVGroupIDMetric, LOVIncludeBlankOnTop, AllowOverrideLOVValue, UnitLOVGroupID, UnitLOVGroupIDMetric, UnitLOVIncludeBlankOnTop, UnitDisplay, UnitDisplayPosition, DefaultValue, DefaultValueMetric, DefaultUnitValue, DefaultUnitValueMetric, AutoInsertValue, AutoInsertValueMetric, AutoInsertUnit, AutoInsertUnitMetric, HasMetric, IECorANSIFormat, cast(isEditabilityVisibilityTrigger as bit) as isEditabilityVisibilityTrigger, EditabilityVisibilityLogic, EditabilityVisibilityPositiveEffect, EditabilityVisibilityNegativeEffect, LibraryID, cast(isNodeBusProperty as bit) as isNodeBusProperty FROM ComponentProperties cp "
                + " WHERE cp.ComponentID = @ComponentID    "
                + " ORDER BY cp.DisplayOrderInComponentEditor,  "
                + "          cp.ShortName;  "
                + "     "
                + "     "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        lov.LOVID ,  "
                + "        lov.Text ,   "
                + "        lov.Value ,  "
                + "        lov.ExportImportAs , "
                + "        lov.DisplayOrder ,   "
                + "        lov.isActive ,   "
                + "        0 AS isBlankEntry    "
                + " FROM LOVGroups lovg "
                + " INNER JOIN LOVs lov ON (lov.LOVGroupID = lovg.LOVGroupID)   "
                + " WHERE lovg.LOVGroupID IN    "
                + "     (SELECT cp.LOVGroupID   "
                + "      FROM ComponentProperties cp    "
                + "      WHERE cp.ComponentID = @ComponentID   "
                + "        AND cp.LOVGroupID IS NOT NULL)   "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        NULL AS LOVID ,  "
                + "        NULL AS Text ,   "
                + "        NULL AS Value ,  "
                + "        NULL AS ExportImportAs , "
                + "        NULL AS DisplayOrder ,   "
                + "        NULL AS isActive ,   "
                + "        1 AS isBlankEntry    "
                + " FROM ComponentProperties cp "
                + " INNER JOIN LOVGroups lovg ON (lovg.LOVGroupID = cp.LOVGroupID)  "
                + " WHERE cp.ComponentID = @ComponentID    "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        lov.LOVID ,  "
                + "        lov.Text ,   "
                + "        lov.Value ,  "
                + "        lov.ExportImportAs , "
                + "        lov.DisplayOrder ,   "
                + "        lov.isActive ,   "
                + "        0 AS isBlankEntry    "
                + " FROM LOVGroups lovg "
                + " INNER JOIN LOVs lov ON (lov.LOVGroupID = lovg.LOVGroupID)   "
                + " WHERE lovg.LOVGroupID IN    "
                + "     (SELECT cp.LOVGroupIDMetric "
                + "      FROM ComponentProperties cp    "
                + "      WHERE cp.ComponentID =@ComponentID    "
                + "        AND cp.LOVGroupIDMetric IS NOT NULL) "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        NULL AS LOVID ,  "
                + "        NULL AS Text ,   "
                + "        NULL AS Value ,  "
                + "        NULL AS ExportImportAs , "
                + "        NULL AS DisplayOrder ,   "
                + "        NULL AS isActive ,   "
                + "        1 AS isBlankEntry    "
                + " FROM ComponentProperties cp "
                + " INNER JOIN LOVGroups lovg ON (lovg.LOVGroupID = cp.LOVGroupIDMetric)    "
                + " WHERE cp.ComponentID = @ComponentID    "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        lov.LOVID ,  "
                + "        lov.Text ,   "
                + "        lov.Value ,  "
                + "        lov.ExportImportAs , "
                + "        lov.DisplayOrder ,   "
                + "        lov.isActive ,   "
                + "        0 AS isBlankEntry    "
                + " FROM LOVGroups lovg "
                + " INNER JOIN LOVs lov ON (lov.LOVGroupID = lovg.LOVGroupID)   "
                + " WHERE lovg.LOVGroupID IN    "
                + "     (SELECT cp.UnitLOVGroupID   "
                + "      FROM ComponentProperties cp    "
                + "      WHERE cp.ComponentID = @ComponentID   "
                + "        AND cp.UnitLOVGroupID IS NOT NULL)   "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        NULL AS LOVID ,  "
                + "        NULL AS Text ,   "
                + "        NULL AS Value ,  "
                + "        NULL AS ExportImportAs , "
                + "        NULL AS DisplayOrder ,   "
                + "        NULL AS isActive ,   "
                + "        1 AS isBlankEntry    "
                + " FROM ComponentProperties cp "
                + " INNER JOIN LOVGroups lovg ON (lovg.LOVGroupID = cp.UnitLOVGroupID)  "
                + " WHERE cp.ComponentID = @ComponentID    "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        lov.LOVID ,  "
                + "        lov.Text ,   "
                + "        lov.Value ,  "
                + "        lov.ExportImportAs , "
                + "        lov.DisplayOrder ,   "
                + "        lov.isActive ,   "
                + "        0 AS isBlankEntry    "
                + " FROM LOVGroups lovg "
                + " INNER JOIN LOVs lov ON (lov.LOVGroupID = lovg.LOVGroupID)   "
                + " WHERE lovg.LOVGroupID IN    "
                + "     (SELECT cp.UnitLOVGroupIDMetric "
                + "      FROM ComponentProperties cp    "
                + "      WHERE cp.ComponentID = @ComponentID   "
                + "        AND cp.UnitLOVGroupIDMetric IS NOT NULL) "
                + " UNION   "
                + " SELECT lovg.LOVGroupID ,    "
                + "        lovg.LOVGroupName ,  "
                + "        NULL AS LOVID ,  "
                + "        NULL AS Text ,   "
                + "        NULL AS Value ,  "
                + "        NULL AS ExportImportAs , "
                + "        NULL AS DisplayOrder ,   "
                + "        NULL AS isActive ,   "
                + "        1 AS isBlankEntry    "
                + " FROM ComponentProperties cp "
                + " INNER JOIN LOVGroups lovg ON (lovg.LOVGroupID = cp.UnitLOVGroupIDMetric)    "
                + " WHERE cp.ComponentID = @ComponentID    "
                + " ORDER BY lovg.LOVGroupID,   "
                + "          isBlankEntry DESC, "
                + "          DisplayOrder,  "
                + "          lov.Text   "},
            {"GetValidationDetailByIDByUseMetric","select cp.ComponentID as ComponentID, cpv.ComponentPropertyID as ComponentPropertyID, cpv.ValidationID as ValidationID, cpv.isForMetric as isForMetric, cpv.ValidationOrder as ValidationOrder, cpv.StopWhenFailed as StopWhenFailed , v.ValidationName as ValidationName, v.RegularExpression as RegularExpression, v.ValidationLogic as ValidationLogic, v.Severity as Severity, v.Text as Text from componentPropertyValidations cpv left join validations v on cpv.ValidationID = v.ValidationID left join componentproperties cp on cp.componentpropertyid = cpv.componentpropertyid where cpv.ComponentPropertyID = @ComponentPropertyID and cpv.isForMetric = @UseMetric order by cpv.ValidationOrder asc"},
            {"UpdateProjectComponentPropertyByIDs","update ProjectComponentProperties set isDataNull=@isDataNull, Value=@Value,isUnitNull=@isUnitNull,UnitValue = @UnitValue where ComponentPropertyID = @ComponentPropertyID and ProjectComponentID=@ProjectComponentID"},
            {"UpdateProjectComponentNameByID","update projectcomponents set ProjectComponentName=@ComponentName where projectcomponentid=@ProjectComponentID"},
            {"GetProjectComponentsByNameNComponentID","select  ProjectComponentName from projectcomponents where ProjectComponentName=@ComponentName  and ProjectComponentID != @ProjectComponentID "},
            {"GetNextComponentNameByID", "SELECT ComponentID,ComponentPrefix FROM ComponentOverrides where ComponentID=@ComponentID"},
            {"GetProjectPropertyByProperty","select Value  from projectproperties where Property=@Property "},
            {"GetAppPropertyByProperty","select Value from applicationProperties where Property=@Property "},
            {"GetComponentPrefixLastUsedSequenceNoByComponentPrefix","select Componentprefix,LastUsedSequenceNo from ComponentPrefixLastUsedSequenceNo where componentprefix=@ComponentPrefix;"},
            {"GetProjectComponentsByComponentName","select  ProjectComponentName from projectcomponents where ProjectComponentName=@ComponentName  "},
            {"UpdateComponentPrefixLastUsedSequenceNoByComponentPrefix","update ComponentPrefixLastUsedSequenceNo set LastUsedSequenceNo=@LastUsedSequenceNo where componentprefix=@ComponentPrefix"},
            {"InsertComponentPrefixLastUsedSequenceNo","INSERT INTO ComponentPrefixLastUsedSequenceNo VALUES(@ComponentPrefix,@LastUsedSequenceNo)"},
            {"InsertProjectComponents","INSERT INTO ProjectComponents(ProjectComponentid, ComponentID, ProjectComponentname) values(@ProjectComponentID, @ComponentID,@NextComponentName);"},
            {"InsertProjectComponentAttachments","insert into ProjectComponentAttachments (ProjectComponentAttachmentID,ProjectComponentID,AttachmentType,Filename,Note,StorageLocation,ImageData,OriginalFilename)values (@ProjectComponentAttachmentID,@ProjectComponentID,@AttachmentType,@Filename,@Note,@StorageLocation,@ImageData,@OriginalFilename);"},
            {"GetComponentAttachments","select ProjectComponentAttachmentID,ProjectComponentID,AttachmentType,Filename,Note,StorageLocation,ImageData from projectcomponentAttachments where projectcomponentid=@projectcomponentid;"},
            {"DeleteProjectComponent","Delete from ProjectComponents where ProjectComponentID=@projectcomponentid ;Delete from projectcomponentproperties where ProjectComponentID=@projectcomponentid; Delete from projectcomponentAttachments where projectComponentId=@projectcomponentid;"},
            {"DeleteAttachments","delete from ProjectComponentAttachments where  projectcomponentattachmentid in (@projectcomponentattachmentids);"},
            {"GetTopComponentConnectionID","SELECT * FROM ComponentConnections WHERE ComponentID = @ComponentID ORDER BY ConnectionToPriority;"},
            {"GetComponentConnectionByComponentConnectionID","SELECT * FROM ComponentConnections WHERE ComponentConnectionID = @ComponentConnectionID"},
            {"InsertProjectComponentConnections","INSERT INTO ProjectComponentConnections (ProjectComponentConnectionID, ProjectComponentID1, ComponentConnectionID1, ProjectComponentID2, ComponentConnectionID2, LastViewOnBranchview1,LastViewOnBranchview2) VALUES (@ProjectComponentConnectionID, @ProjectComponentID1, @ComponentConnectionID1, @ProjectComponentID2, @ComponentConnectionID2, @LastViewOnBranchview1,@LastViewOnBranchview2);"},
            {"GetAllConnection",@"SELECT  ComponentConnectionID As TargetComponentConnectionID
  FROM ComponentConnections 
 WHERE ComponentID = @TargetComponentID
 ORDER BY ConnectionToPriority limit 1
"},
            {"GetCurrentConnections",@"insert into app.ExistingConnections        
                SELECT pc.ComponentID
                      ,pc.ProjectComponentID
                      ,pc.ProjectComponentName
                      ,ucc.ComponentConnectionID
                      ,ucc.Label As ConnectionLabel
                      ,ucc.DisplayOrder As ConnectionDisplayOrder
                      ,'D' As Connection
                      ,dpc.ComponentID As ConnectedComponentID
                      ,dpc.ProjectComponentID As ConnectedProjectComponentID
                      ,dpc.ProjectComponentName As ConnectedProjectComponentName
                      ,dcc.ComponentConnectionID As ConnectedComponentConnectionID
                      ,dcc.Label As ConnectedConnectionLabel
                      ,dcc.DisplayOrder As ConnectedConnectionDisplayOrder
                      ,pcc.CreationOrder


                  FROM prj.ProjectComponents pc
                 INNER JOIN prj.ProjectComponentConnections pcc ON (pcc.UpstreamProjectComponentID = pc.ProjectComponentID) -- Looking for my downstream so I am the upstream component
                 INNER JOIN prj.ProjectComponents dpc ON (dpc.ProjectComponentID = pcc.DownstreamProjectComponentID)        -- Looking for my downstream
                  LEFT JOIN app.ComponentConnections dcc ON (dcc.ComponentConnectionID = pcc.DownstreamComponentConnectionID)
                  LEFT JOIN app.ComponentConnections ucc ON (ucc.ComponentConnectionID = pcc.UpstreamComponentConnectionID)
                 WHERE pc.ProjectComponentID = @ProjectComponentID;

        select* from app.ExistingConnections
       UNION
                -- Get upstream of current component.So current component is the downstream component
              SELECT pc.ComponentID
                      ,pc.ProjectComponentID
                      ,pc.ProjectComponentName
                      ,dcc.ComponentConnectionID
                      ,dcc.Label As ConnectionLabel
                      ,dcc.DisplayOrder As ConnectionDisplayOrder
                      ,'U' As Connection
                      , upc.ComponentID As ConnectedComponentID
                      ,upc.ProjectComponentID As ConnectedProjectComponentID
                      ,upc.ProjectComponentName As ConnectedProjectComponentName
                      ,ucc.ComponentConnectionID As ConnectedComponentConnectionID
                      ,ucc.Label As ConnectedConnectionLabel
                      ,ucc.DisplayOrder As ConnectedConnectionDisplayOrder
                      ,pcc.CreationOrder
                  FROM prj.ProjectComponents pc
                 INNER JOIN app.Components p ON(p.ComponentID = pc.ComponentID)
                 INNER JOIN prj.ProjectComponentConnections pcc ON(pcc.DownstreamProjectComponentID = pc.ProjectComponentID)   -- Looking for my uptream so I am the downstream component
                 INNER JOIN prj.ProjectComponents upc ON(upc.ProjectComponentID = pcc.UpstreamProjectComponentID)              -- Looing for my upstream
                  LEFT JOIN app.ComponentConnections ucc ON(ucc.ComponentConnectionID = pcc.UpstreamComponentConnectionID)
                  LEFT JOIN app.ComponentConnections dcc ON(dcc.ComponentConnectionID = pcc.DownstreamComponentConnectionID)
                 WHERE pc.ProjectComponentID == @ProjectComponentID;

 
 -- Table 0
                    SELECT c.ComponentID
                          , c.ComponentName As Component
                          , pc.ProjectComponentID
                          , pc.ProjectComponentName
                          , c.NoOfDownstreamConnections
                          , c.NoOfUpstreamConnections
                          , c.ComponentConnectionSpecialLogic
                          , IFNULL(ec.NoOfConnectedUpstreamConnection, 0) As NoOfConnectedUpstreamConnection
                           , IFNULL(ec.NoOfConnectedDownstreamConnection, 0) As NoOfConnectedDownstreamConnection
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID)
                      LEFT JOIN(SELECT SUM(CASE
                                                WHEN Connection = 'U' THEN 1
                                                ELSE 0
                                            END) As NoOfConnectedUpstreamConnection
                                       , SUM(CASE
                                                WHEN Connection = 'D' THEN 1
                                                ELSE 0
                                            END) As NoOfConnectedDownstreamConnection
                                   FROM ExistingConnections) ec ON(1=1)
                     WHERE pc.ProjectComponentID = @ProjectComponentID;

                     
                    -- Table 1
                    SELECT*
                      FROM ExistingConnections
                     ORDER BY Connection, ConnectedProjectComponentName;

                    -- Table 2 - open connections
                    -- Regular components
                    SELECT pc.ProjectComponentID
                          , pc.ProjectComponentName
                          ,'U' As Connection
                          , cc.Label
                          , cc.DisplayOrder
                          , c.ComponentConnectionSpecialLogic
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID)
                      LEFT JOIN app.ComponentConnections cc ON(cc.ComponentID = c.ComponentID
                                                            AND cc.isForUpstream = 1)
                      -- Exclude existing connections
                      LEFT JOIN ExistingConnections ec ON(ec.Connection = 'U'
                                                        AND IFNULL(CAST(ec.ComponentConnectionID as varchar(100)), '') = IFNULL(CAST(cc.ComponentConnectionID as varchar(100)), ''))
                     WHERE pc.ProjectComponentID = @ProjectComponentID
                       AND ec.ProjectComponentID IS NULL            -- No existing connections
                       AND c.NoOfUpstreamConnections<> 9999        -- Non Bus
                       AND c.ComponentConnectionSpecialLogic<> 1   -- Not a generator type
                     UNION
                    SELECT pc.ProjectComponentID
                          , pc.ProjectComponentName
                          ,'D' As Connection
                          , cc.Label
                          , cc.DisplayOrder
                          , c.ComponentConnectionSpecialLogic
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID)
                      LEFT JOIN app.ComponentConnections cc ON(cc.ComponentID = c.ComponentID
                                                            AND cc.isForDownstream = 1)
                      -- Exclude existing connections
                      LEFT JOIN ExistingConnections ec ON(ec.Connection = 'D'
                                                        AND IFNULL(CAST(ec.ComponentConnectionID as varchar(100)), '') = IFNULL(CAST(cc.ComponentConnectionID as varchar(100)), ''))
                     WHERE pc.ProjectComponentID = @ProjectComponentID
                       AND ec.ProjectComponentID IS NULL            -- No existing connections
                       AND c.NoOfDownstreamConnections<> 9999      -- Non Bus
                       AND c.ComponentConnectionSpecialLogic<> 1   -- Not a generator type
                     UNION
                    -- Buses
                    SELECT pc.ProjectComponentID
                          , pc.ProjectComponentName
                          ,'D' As Connection
                          , cc.Label
                          , cc.DisplayOrder
                          , c.ComponentConnectionSpecialLogic
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID)
                      LEFT JOIN app.ComponentConnections cc ON(cc.ComponentID = c.ComponentID
                                                            AND cc.isForDownstream = 1)
                     WHERE pc.ProjectComponentID = @ProjectComponentID
                       AND c.NoOfDownstreamConnections = 9999-- Bus devices
                     UNION
                    SELECT pc.ProjectComponentID
                          , pc.ProjectComponentName
                          ,'U' As Connection
                          , cc.Label
                          , cc.DisplayOrder
                          , c.ComponentConnectionSpecialLogic
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID)
                      LEFT JOIN app.ComponentConnections cc ON(cc.ComponentID = c.ComponentID
                                                            AND cc.isForUpstream = 1)
                     WHERE pc.ProjectComponentID = @ProjectComponentID
                       AND c.NoOfDownstreamConnections = 9999-- Bus devices
                     UNION
                    -- Generator
                    SELECT pc.ProjectComponentID
                          , pc.ProjectComponentName
                          ,'D' As Connection
                          , Null As Label
                          , Null As DisplayOrder
                          ,0 As ComponentConnectionSpecialLogic
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID
                                                 AND c.ComponentConnectionSpecialLogic = 1)
                      -- Exclude existing connections
                      LEFT JOIN ExistingConnections ec ON(ec.Connection IN ('D', 'U'))
                     WHERE pc.ProjectComponentID = @ProjectComponentID
                       AND ec.ProjectComponentID IS NULL        -- No current connection
                     UNION
                    SELECT pc.ProjectComponentID
                          , pc.ProjectComponentName
                          ,'U' As Connection
                          , Null As Label
                          , Null As DisplayOrder
                          , c.ComponentConnectionSpecialLogic
                      FROM prj.ProjectComponents pc
                     INNER JOIN app.Components c ON (c.ComponentID = pc.ComponentID
                                                 AND c.ComponentConnectionSpecialLogic = 1)
                      -- Exclude existing connections
                      LEFT JOIN ExistingConnections ec ON(ec.Connection IN ('D', 'U'))
                     WHERE pc.ProjectComponentID = @ProjectComponentID
                       AND ec.ProjectComponentID IS NULL;



                            delete from  ExistingConnections;
                      "},
            {"UpdateComponentEditorResizeableSize",@"UPDATE PROJECTPROPERTIES SET Value=@left WHERE Property='ComponentEditorResizeableWidthLeft';
                UPDATE PROJECTPROPERTIES SET Value=@right WHERE Property='ComponentEditorResizeableWidthRight';"},
            {"GetComponentEditorResizeableSizes",@"SELECT Property,Value,Description FROM PROJECTPROPERTIES 
                WHERE Property IN('ComponentEditorResizeableWidthLeft','ComponentEditorResizeableWidthRight');"},
              {"DisconnectConnection","DELETE FROM ProjectComponentConnections WHERE ProjectComponentConnectionID=@ProjectComponentConnectionID"},
            {"GetComponentsWithImage","SELECT c.ComponentId,c.ComponentName,g.ImageFileName FROM Components c  INNER JOIN ComponentGraphics g on c.ComponentId=g.ComponentId ORDER BY c.ComponentName;"},
            {"GetProjectGraphicsMode","SELECT Value from projectproperties WHERE Property='IECorANSIFormat'"},
            {"GetComponentGraphics",@"select c.ComponentID,c.ComponentName,g.IECorANSIFormat,g.ImageFileName from Components c
                    inner join ComponentGraphics g on g.ComponentID = c.ComponentID and c.ComponentID =@ComponentID
                    where g.IECorANSIFormat = @GraphicsMode
                    or g.IECorANSIFormat  =0;

                     select DISTINCT c.ComponentID,c.ComponentConnectionID,
                     case when @GraphicsMode=2 or g.IECorANSIFormat = 0  then c.ANSIConnectionX else c.IECConnectionX end  as ConnectionX,
                    case when @GraphicsMode=2 or g.IECorANSIFormat = 0  then c.ANSIConnectionY else c.IECConnectionY end  as ConnectionY, 
                    case when @GraphicsMode=2 or g.IECorANSIFormat = 0  then c.ANSIConnectionRadius else c.IECConnectionRadius end  as ConnectionRadius 
					from Componentconnections  c 
inner join ComponentGraphics g on g.ComponentID = c.ComponentID              
where c.ComponentID=@ComponentID"}
        };
    }
}

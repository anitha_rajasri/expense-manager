﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace MoneyManagement.Shared.Interfaces
{
    public interface IDBService
    {
        string ActiveConnectionString
        {
            get;
        }

        void ConnectionString();

        void Initialize(string StoreProcedureName = "", string Query = "", Dictionary<string, List<string>> replaceExistingParameter = null);

        void AddParameter(string ParameterName, object Value);

        void AddOutputParameter(string ParameterName, Int32 Size);

        DataSet GetDataSet();
        DataTable GetDataTable();
        DataRow GetDataRow();

        DataSet GetDataSet(int CommandTimeout);


        object ExecuteStoreProcedure(string ReturnParameterName);

        Task<int> ExecuteNonQueryAsync();

        void BeginTransaction(System.Data.IsolationLevel iso = IsolationLevel.Unspecified);

        void CommitTransaction();

        void RollbackTransaction();

        object IsNull(object field, object ReturnNullAs);

        string OutputCallConfiguration();
    }
}

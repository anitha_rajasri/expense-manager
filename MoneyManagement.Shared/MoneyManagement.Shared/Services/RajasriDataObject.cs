﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoneyManagement.Shared.Helper;
using MoneyManagement.Shared.Interfaces;
using SQLite.Net;
using System.data

namespace MoneyManagement.Shared
{

    public class RajasriDataObject : IDBService
    {
        private SQLiteConnection _dbConnection;
        private SQLiteCommand _sqlCommand;
        private DataSet _sqlDataSet;
        private SqliteTransaction _sqlTransaction;
        string IDBService.ActiveConnectionString { get => _dbConnection?.ConnectionString; }

        public void ConnectionString()
        {
            _dbConnection = new SqliteConnection(ConfigurationManager.GetConnectionString());
        }

        public void Initialize(string StoreProcedureName = "", string Query = "", Dictionary<string, List<string>> replaceExistingParameter = null)
        {
            if ((string.IsNullOrEmpty(StoreProcedureName) && string.IsNullOrEmpty(Query)) || (!string.IsNullOrEmpty(StoreProcedureName) && !string.IsNullOrEmpty(Query)))
            {
                throw new InvalidConstraintException("Should has any one. Either StoreProcedureName or Query", new Exception());
            }
            if (!string.IsNullOrEmpty(StoreProcedureName))
            {
                string query = StoreProcedures.StoreProcedure[StoreProcedureName];

                if (replaceExistingParameter != null && replaceExistingParameter.Count > 0)
                {
                    //this part is for adding "where IN parameter values"
                    foreach (string key in replaceExistingParameter.Keys)
                    {
                        query = query.Replace(key, string.Join(",", replaceExistingParameter[key].ToList<string>()));
                    }
                }
                _sqlCommand = new SqliteCommand(query, _dbConnection);
            }
            else
                _sqlCommand = new SqliteCommand(Query, _dbConnection);

            _sqlCommand.CommandType = CommandType.Text;
            if ((_sqlTransaction != null))
            {
                _sqlCommand.Transaction = _sqlTransaction;
            }
            _sqlDataSet = null;
        }


        public void AddParameter(string ParameterName, object Value)
        {
            _sqlCommand.Parameters.Add(new SqliteParameter("@" + ParameterName, Value));
        }

        public void AddOutputParameter(string ParameterName, Int32 Size)
        {
            SqliteParameter param = new SqliteParameter();
            param.ParameterName = ParameterName;
            param.Size = Size;
            param.Direction = ParameterDirection.Output;
            if (Size == 4)
                param.DbType = DbType.Int32;
            _sqlCommand.Parameters.Add(param);
        }

        public DataSet GetDataSet()
        {
            SqliteDataAdapter adaptor = new SqliteDataAdapter(_sqlCommand);
            _sqlDataSet = new DataSet();
            adaptor.Fill(_sqlDataSet, _sqlCommand.CommandText);
            return _sqlDataSet;

        }

        public DataTable GetDataTable()
        {
            return GetDataSet().Tables[0];
        }

        public DataRow GetDataRow()
        {
            return GetDataSet().Tables[0].Rows[0];
        }

        public DataSet GetDataSet(int CommandTimeout)
        {

            SqliteDataAdapter adaptor = new SqliteDataAdapter(_sqlCommand);
            _sqlDataSet = new DataSet();
            _sqlCommand.CommandTimeout = CommandTimeout;
            adaptor.Fill(_sqlDataSet, _sqlCommand.CommandText);
            return _sqlDataSet;

        }

        public object SqlDataSet
        {
            get { return _sqlDataSet; }
        }



        public object ExecuteStoreProcedure(string ReturnParameterName)
        {

            bool bConnectionWasClosed = _dbConnection.State == ConnectionState.Closed;

            if (bConnectionWasClosed)
                _dbConnection.Open();
            _sqlCommand.ExecuteNonQuery();
            if (bConnectionWasClosed)
                _dbConnection.Close();

            SqliteParameter param = _sqlCommand.Parameters[ReturnParameterName];
            return param.Value;

        }

        public async Task<int> ExecuteNonQueryAsync()
        {
            bool bConnectionWasClosed = _dbConnection.State == ConnectionState.Closed;

            if (bConnectionWasClosed)
                _dbConnection.Open();
            int affected_count = await _sqlCommand.ExecuteNonQueryAsync();
            if (bConnectionWasClosed)
                _dbConnection.Close();
            return affected_count;
        }


        public void BeginTransaction(System.Data.IsolationLevel iso = IsolationLevel.Unspecified)
        {
            // Refer http://msdn.microsoft.com/en-us/library/system.data.isolationlevel.aspx 


            if (_sqlTransaction == null)
            {
                _dbConnection.Open();
                _sqlTransaction = _dbConnection.BeginTransaction(iso);
            }
            else
            {
                Exception ex = new Exception("SQL transaction already exist");
                throw ex;
            }
        }

        public void CommitTransaction()
        {
            _sqlTransaction.Commit();
            _sqlTransaction = null;
            _dbConnection.Close();
        }

        public void RollbackTransaction()
        {
            if (_sqlTransaction != null)
            {
                _sqlTransaction.Rollback();
                _sqlTransaction = null;
                _dbConnection.Close();
            }
        }

        public object IsNull(object field, object ReturnNullAs)
        {

            if (DBNull.Value.Equals(field))
            {
                return ReturnNullAs;
            }
            else
            {
                return field;
            }
        }

        public string OutputCallConfiguration()
        {

            StringBuilder s = new StringBuilder("\r\n" + "\r\n" + "***********************" + "\r\n");
            //SqlParameter param = default(SqlParameter);

            s.Append("Procedure name: " + _sqlCommand.CommandText + "\r\n");
            foreach (SqlParameter param in _sqlCommand.Parameters)
            {
                if (param.Direction == ParameterDirection.Input | param.Direction == ParameterDirection.InputOutput)
                {
                    s.Append(param.ParameterName + " = " + param.Value.ToString() + "\r\n");
                }
                else
                {
                    s.Append(param.ParameterName + " OUTPUT" + "\r\n");
                }
            }
            return s.ToString();

        }

        public void ConnectionString()
        {
            throw new NotImplementedException();
        }
    }
}
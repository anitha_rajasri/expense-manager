﻿using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.OS;
using static Android.Bluetooth.BluetoothClass;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using static Java.Util.ResourceBundle;

namespace MoneyManagement.Droid
{
    [Activity(Label = "Expense Manager", Icon = "@drawable/logo", Theme = "@style/MainTheme", MainLauncher = false, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait )]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
           
            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                Window.SetStatusBarColor(Android.Graphics.Color.Rgb(248, 75, 95));

        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyManagement
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingPage : ContentPage
    {
        string page = "SettingDetailsPage";
        private List<PersonList> _listOfPeople;
        

        public List<PersonList> ListOfPeople { get { return _listOfPeople; } set { _listOfPeople = value; base.OnPropertyChanged(); } }

        public SettingPage()
        {
            InitializeComponent();


        }



        protected override void OnAppearing()
        {
            base.OnAppearing();

            var sList = new PersonList()
        {
            new Settinglist() { Settingitem = "Security", itemid=01},
             //  new Settinglist() { Settingitem = "Location" ,itemid=11},
             //new Settinglist() { Settingitem = "Language&input",itemid=21 }
        };
            sList.Heading = "Private";

          /*  var dList = new PersonList()
            {
            new Settinglist() { Settingitem = "Jane",  itemid=02}
            };
            dList.Heading = "D";

            var jList = new PersonList()
            {
             new Settinglist() { Settingitem = "Billy" }
            };
            jList.Heading = "J";*/

            var list = new List<PersonList>()
    {
        sList,
       
       
    };
            List.ItemsSource = list;
           
        }

        private void SearchResults_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = (Settinglist)e.SelectedItem;
            int id = item.itemid;
            if (id == 01)
            {
                Security();
              //  Navigation.PushModalAsync(new SettingDetailsPage());
            };
              
        }

        public async void Security()
        {
          //  Navigation.PushAsync(new SettingDetailsPage());

             var userlist = await App.Database.GetpasswordAsync();
            if (userlist == null)
            {
                Navigation.PushAsync(new SettingDetailsPage());
            }
             else if(userlist.login_password == null)
              {
                  Navigation.PushAsync(new SettingDetailsPage());
              }
              else
              {
                  var LoginPage = new LoginPage(page);
                  Navigation.PushAsync(LoginPage);

                  if (await LoginPage.CompletedTask == true)
                  {

                    //  DisplayAlert("Login Success", "Success", "Cancel");

                  }
                  else
                  {
                      DisplayAlert("Login Failed", "Failed", "Cancel");
                  }

              }


        }
       
    }
    }

	
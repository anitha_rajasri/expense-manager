﻿using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyManagement
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuMasterDetailsPage:MasterDetailPage
	{
        public MenuMasterDetailsPage ()
		{
			InitializeComponent ();
            navigationDrawerList.ItemsSource = GetMasterPageLists();
           
          
           
        }
        
       private void OnMenuSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (MasterPageList)e.SelectedItem;

            if (item.Title == "Home")
            {
                Detail.Navigation.PushAsync(new HomeTabbedPage());
                IsPresented = false;
            }
           else if(item.Title== "Add Income")
            {
                Title = "SaveIncome";
                string categorytype = "Income";

                Detail.Navigation.PushAsync(new IncomeandExpensePage(Title,categorytype,0,"","",0));
                IsPresented = false;
            }
            else if (item.Title == "Add Expense")
            {
                Title = "SaveEpense";
                string categorytype = "Expense";
                Detail.Navigation.PushAsync(new IncomeandExpensePage(Title,categorytype,0,"","",0));
                IsPresented = false;
            }



        }


        List<MasterPageList> GetMasterPageLists()
        {
            return new List<MasterPageList>
            {
             new MasterPageList() { Title = "Home" },
             new MasterPageList() { Title = "Add Income", },
             new MasterPageList() { Title = "Add Expense", },
             new MasterPageList() { Title=  "Security"},
             new MasterPageList() {  Title= "Settings"}
        };
           
        }


    }
    public class MasterPageList
    {

        public string Title { get; set; }
    }
    
}
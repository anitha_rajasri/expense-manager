﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyManagement
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingDetailsPage : ContentPage
    {
        string Authendication;
        string page = "SettingDetailsPage";
        public SettingDetailsPage()
        {


           

             InitializeComponent();
             UpdatePage();


        }
         public async void UpdatePage()
           {
                var lst = await App.Database.GetpasswordAsync();
              if(lst==null)
               {
              
               }
              else if (lst.login_password == null)
               {

                Enableswitch.IsToggled = false;

                }
               else
               {

               
                Enableswitch.IsToggled = true;
               
               }

         }


           
          void Handle_Toggled(object sender, Xamarin.Forms.ToggledEventArgs e)
           {
          
            if (Enableswitch.IsToggled)
            {

                Passwordentry.IsEnabled = true;
              //  Enableswitch.IsToggled = true;
            }
            else
            {

                Passwordentry.IsEnabled = false;
              //  Enableswitch.IsToggled = false;
            }
        }
        
        private async void Savebtn_Clicked(object sender, EventArgs e)
        {
            if(!Enableswitch.IsToggled && Passwordentry.Text==null )
            {
               
                var lst = await App.Database.GetpasswordAsync();
                if(lst==null)
                {
                    TBL_user tblobj = new TBL_user();

                    tblobj.login_Security = "Disable";                  
                    tblobj.login_password = null;
                   
                    tblobj.Created_date = DateTime.Now.ToString("MM/dd/yyyy");
                    App.Database.SaveItemAsync(tblobj);
                    savebtn.Text = "Updated!";
                   

                }
                else
                {
                    TBL_user tblobj = new TBL_user();
                    tblobj.login_Security = "Disable";
                    tblobj.login_id = lst.login_id;
                    tblobj.login_password = null;
                    tblobj.Created_date = DateTime.Now.ToString("MM/dd/yyyy");
                    App.Database.SaveItemAsync(tblobj);
                    savebtn.Text = "Updated!";
                   
                }
            }
            else if(Passwordentry.Text==null || Passwordentry.Text.Length <4)
            {
                DisplayAlert("Alert", "password not null and must be 4 digits", "ok");

            }
            else if(Enableswitch.IsToggled && Passwordentry.Text.Length>=4)
            {
                var lst = await App.Database.GetpasswordAsync();
                if (lst == null)
                {
                    TBL_user tblobj = new TBL_user();
                    tblobj.login_Security = "Enable";
                    
                    tblobj.login_password = Passwordentry.Text;
                    tblobj.Created_date = DateTime.Now.ToString("MM/dd/yyyy");
                    App.Database.SaveItemAsync(tblobj);
                    savebtn.Text = "Updated!";
                }
                else
                {
                    TBL_user tblobj = new TBL_user();
                    tblobj.login_id = lst.login_id;
                    tblobj.login_Security = "Enable";
                    tblobj.login_password = Passwordentry.Text;
                    tblobj.Created_date = DateTime.Now.ToString("MM/dd/yyyy");
                    App.Database.SaveItemAsync(tblobj);
                    savebtn.Text = "Updated!";

                }
            }


        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;

namespace MoneyManagement
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IncomeandExpensePage : ContentPage
    {
        DateTime selecdedate;
        int Selectednameid;
        private string categorytype;
        private int  Id;
        string name;
        public IncomeandExpensePage(string title, string categorytype,double amount,string name,string date,int id  )
        {

            InitializeComponent();
            Clear();
            Title = title;
            this.categorytype = categorytype;
            CategorynamelistAsync();
            this.Id = id;

            datedentry.Date = DateTime.Parse(date);

            if (Title=="Edit Income"||Title== "Edit Expense")
            {
                CategorynamelistAsync(name);
                amountentry.Text = amount.ToString();
                datedentry.Date = Convert.ToDateTime(date);
               
                savebtn.Text = "Update";
                backbtn.Text = "Cancel";
                


            }
        }
       
        private void Addbtn_Clicked(object sender, EventArgs e)
        {
            IsToShowPopup(true);
            categoryname.Focus();
        }

        private void IsToShowPopup(bool IsToShow)
        {
            overlayview.IsVisible = IsToShow;
            popupLoginView.IsVisible = IsToShow;
        }

        public void pickervalue(string categoryname)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                name = categoryname;
                picker.SelectedItem = name;
            });

        }

        public async void CategorynamelistAsync(string catename = null)
        {
            List<string> inomecategory = new List<string>();
            Debug.WriteLine("CategorynamelistAsync is called with catename => " + catename);

            var incomelist = await App.Database.GetcategorylistAsync(categorytype);
            Debug.WriteLine("CategorynamelistAsync incomelist retrived count  => " + incomelist.Count);
           Device.BeginInvokeOnMainThread(() =>
           {
               Debug.WriteLine("CategorynamelistAsync.BeginInvokeOnMainThread is called");
              
                foreach (TBL_category name in incomelist)
                {
                   Task.Delay(2000);
                   inomecategory.Add(name.category_name.ToString());
                      
                }
               Debug.WriteLine($"CategorynamelistAsync.BeginInvokeOnMainThread inomecategory.count => {inomecategory.Count} => catename =>{catename}");

                picker.ItemsSource = inomecategory;

               if ((catename != null && Title == "Edit Income") || (catename != null && Title == "Edit Expense"))
               {
                   if (inomecategory.Contains(catename))
                       picker.SelectedItem = catename;
                   else

                   picker.SelectedItem = catename.First().ToString().ToLower() + catename.Substring(1);
               }
               else
               {
                   picker.SelectedItem = catename;
                 //  picker.SelectedItem = catename.First().ToString().ToLower() + catename.Substring(1);
               }
           });
            Debug.WriteLine("CategorynamelistAsync call is ended");


        }

        private void savebtn_Clicked(object sender, EventArgs e)
        {

            validationAsync();
        }

        void OnSelectedDate(object sender, DateChangedEventArgs args)
        {

            selecdedate = args.NewDate;

        }

        public async void CategoryidAsync(string selectedname)
        {
            var id = await App.Database.GetidAsync(selectedname);
            Selectednameid = id.category_id;

        }

       


        public void Onfocus(object sender,FocusEventArgs focus)

        {
            RelativeLayout.SetYConstraint(popupLoginView, Constraint.RelativeToParent((Parent) =>
            {

                return (0 * Parent.Height);
                Constraint.Constant(10);
            }
            ));
             }


        public void Offfocus(object sender, FocusEventArgs focus)

        {
            RelativeLayout.SetYConstraint(popupLoginView, Constraint.RelativeToParent((Parent) =>
            {

                return (.5 * Parent.Height) - 100;
            }
            ));
        }



        public void Clear()
        {
            amountentry.Text = null;
            picker.SelectedIndex = -1;
            datedentry.Date = DateTime.Now;

        }
        public async void validationAsync()
        {
            string str = amountentry.Text;
            TBL_IncomeandExpenseDetails tblobj = new TBL_IncomeandExpenseDetails();
            if (picker.SelectedIndex == -1 || amountentry.Text == null)
            {
                if (picker.SelectedIndex == -1 && amountentry.Text == null)
                { DisplayAlert("Alert", "Please enter amount and category  ", "ok"); }

               

                else if (amountentry.Text == null) { DisplayAlert("Alert", "Please enter amount", "ok"); }

                else if (picker.SelectedIndex == -1) { DisplayAlert("Alert", "Please select category", "ok"); }
                
            }
            else if (!str.All(char.IsDigit)) { DisplayAlert("Alert", "Please enter valid amount", "ok"); }
            else
            {
                string selectedValue = picker.Items[picker.SelectedIndex];
                var id = await App.Database.GetidAsync(selectedValue);
                int selectedcategoryid = id.category_id;
                if (Title == "Edit Income" ||Title== "Edit Expense")
                    tblobj.IncAndExp_id = Id;

                tblobj.IncAndExp_amount = Convert.ToDouble(amountentry.Text);
                tblobj.IncAndExp_categoryid = selectedcategoryid;
                tblobj.IncAndExp_type = categorytype;
                var date = datedentry.Date;
                
                tblobj.IncAndExp_date = date.ToString("MMMM d, yyyy");
               
                tblobj.IncAndExprecorded_date = DateTime.Now.ToString("MM/dd/yyyy");
                await App.Database.SaveItemAsync(tblobj);
                if(Title== "Save Income" || Title== "Edit Income")
                {
                    IncomePage obj = new IncomePage();
                    obj.refreshData();
                }
                else if(Title == "Save Expense" || Title == "Edit Expense")
                {
                    ExpensePage obj = new ExpensePage();
                    obj.refreshData();
                }

                Navigation.RemovePage(this);
             //   Clear();


            }

        }

     
        private async void Okbtn_Clicked(object sender, EventArgs e)
        {
            int num = 0;
            var lst = await App.Database.getCategoryItemsAsync();
            List<string> namelist = new List<string>();
            bool canConvert = int.TryParse(categoryname.Text, out num);

            foreach (TBL_category name in lst)
            {
                namelist.Add(name.category_name.ToString().ToLower());
                // picker.Items.Add(name.category_name);
            }
            if (categoryname.Text == null)
                DisplayAlert("Alert", "category is null", "ok");

            else if (namelist.Contains(categoryname.Text.ToLower()))
                DisplayAlert("Alert", "categorytype is already exist", "ok");
           
            else if((categoryname.Text.Length > 0 &&
                         categoryname.Text.Trim().Length == 0)||canConvert==true)
                DisplayAlert("Alert", "Please enter valid one", "ok");

            else
            {

                TBL_category tbl = new TBL_category();
                tbl.category_name = categoryname.Text;
                tbl.category_type = categorytype;
                tbl.categoryrecorded_date = DateTime.Now.ToString();
              
                int affectedRows =  await App.Database.SaveItemAsync(tbl);
                Debug.WriteLine($"Okbtn_Clicked affected rows during insert is {affectedRows}");
                CategorynamelistAsync(categoryname.Text);
                categoryname.Text = null; 
                IsToShowPopup(false);


            }
        }

        
        private async void Cancekbtn_Clicked(object sender, EventArgs e)
        {
            IsToShowPopup(false);
            categoryname.Text = null;

        }
        private  void backbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.RemovePage(this);

        }
       
    }
    }
    
    
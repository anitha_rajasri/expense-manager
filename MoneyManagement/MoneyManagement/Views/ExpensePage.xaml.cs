﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyManagement
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpensePage : ContentPage
    {
        int count = 0;
        int entry = 0;
        string filtertype;
        int dayentry = 0;
        bool ispressing = false;
        int popupentry;
        Stopwatch mStopWatch = new Stopwatch();
      

        public ExpensePage()
        {
            try {
            InitializeComponent();
            if (Application.Current.Properties.ContainsKey("filter"))
            {
                var type = Application.Current.Properties["filter"] as string;
                if(type=="month")
                    MonthClicked(new object(), new EventArgs());
                else if(type == "week")
                    WeekClicked(new object(), new EventArgs());
                else if(type=="day")
                    DayClicked(new object(), new EventArgs());
            }
            else
            {
                MonthClicked(new object(), new EventArgs());
            }

            string currentdate = DateTime.Now.ToString("MM/dd/yyyy");
            string currentyear = DateTime.Now.ToString("yyyy");
            string lastdate = string.Concat("12"+"/"+"31"+"/"+ currentyear);
            if (currentdate == lastdate)
            {
                DateTime curyear = Convert.ToDateTime(currentdate);
                string incyear = curyear.AddYears(7).ToString("yyyy");
                pickeryear.Items.Add(incyear); 
                yrpicker.Items.Add(incyear);
                dayyrpicker.Items.Add(incyear);
                pickeryear.Items.Remove(pickeryear.Items[pickeryear.SelectedIndex = 0]);
                yrpicker.Items.Remove(yrpicker.Items[yrpicker.SelectedIndex = 0]);
                dayyrpicker.Items.Remove(dayyrpicker.Items[dayyrpicker.SelectedIndex = 0]);
            }

            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message + Environment.NewLine + e.Message + e.StackTrace);
            }




        }

        void Handle_FabClicked(object sender, System.EventArgs e)
        {
            string Title = "Save Expense";
            string categorytype = "Expense";
            if (filtertype == "month")
            {
                string cmth = pickerMonth.SelectedItem.ToString();
                string cyr = pickeryear.SelectedItem.ToString();
                string currentdate = DateTime.Now.ToString("MMMMyyyy");
                string crntdate = String.Concat(cmth, cyr);
                if (currentdate == crntdate)
                {
                    string datestring = DateTime.Now.ToString();
                    Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", datestring, 0));
                }
                else
                {
                    string crndate = String.Concat(cmth + "/" + "01 " + "/" + cyr);
                    DateTime dates = Convert.ToDateTime(crntdate);
                    string datestr = dates.ToString();
                    Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", datestr, 0));
                }
            }
            else if (filtertype == "week")
            {
                DateTime currentdate = DateTime.Now;
                string weekdate = weekpicker.SelectedItem.ToString();
                string currentyr = yrpicker.SelectedItem.ToString();
                string currentmn = mnthpicker.SelectedItem.ToString();
                string crntdate = String.Concat(currentmn, currentyr);
                string strOutput = weekdate.Split(new char[] { '(', ')' })[1];
                string[] tokens = strOutput.Split('-');
                string concatfstdate = string.Concat(tokens[0] + "," + currentyr);
                string concatlstdate = string.Concat(tokens[1] + "," + currentyr);
                DateTime fstdate = Convert.ToDateTime(concatfstdate);
                DateTime lsdate = Convert.ToDateTime(concatlstdate);
                if (fstdate <= currentdate && currentdate <= lsdate)
                {

                    string datestring = DateTime.Now.ToString();
                    Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", datestring, 0));
                }
                else
                {
                    Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", fstdate.ToString("MM/dd/yyyy"), 0));
                }
            }
            else if (filtertype == "day")
            {
                string dateforday = string.Concat(daymnpicker.SelectedItem.ToString() + "," + daypicker.SelectedItem.ToString() + "," + dayyrpicker.SelectedItem.ToString());
                DateTime day = Convert.ToDateTime(dateforday);
                DateTime currentdate = DateTime.Now;
                if (day == currentdate)
                {
                    string datestring = DateTime.Now.ToString();
                    Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", datestring, 0));
                }
                else
                {
                    string datforday = string.Concat(daymnpicker.SelectedItem.ToString() + "," + "01" + "," + dayyrpicker.SelectedItem.ToString());
                    DateTime da = Convert.ToDateTime(dateforday);
                    Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", da.ToString(), 0));
                }
            }
          //  Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, 0, "", "", 0));

        }
       
        public async void popupmethod(int entry, Listviewmodel Celldata)
        {
            Debug.WriteLine("popupmethod method called");
            await Task.Delay(2000);
            if (ispressing == true && entry == popupentry )
            {
               
                Debug.WriteLine("popup showed successfully");
                var result = await DisplayActionSheet(" Do you want to?", "Cancel", null, "Edit", "Delete");
                if (result == "Edit")
                {

                    string Title = "Edit Expense";
                    string categorytype = "Expense";
                    double amount = Celldata.amount;
                    string categoryname = Celldata.categoryname;
                    string date = Celldata.date;
                    int Id = Celldata.ID;
                    await Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, amount, categoryname, date, Id));


                }
                else if (result == "Delete")
                {

                    var results = await DisplayAlert("You Selected item", "Do you want to delete", "Yes", "No");
                    if (results == true)
                    {
                        await App.Database.delete(Celldata.ID.ToString());

                    }
                    if (filtertype == "month")
                        MonthClicked(new object(), new EventArgs());
                    else if (filtertype == "week")
                        WeekClicked(new object(), new EventArgs());
                    else if (filtertype == "day")
                        DayClicked(new object(), new EventArgs());
                }

                if (filtertype == "month")
                    MonthClicked(new object(), new EventArgs());
                else if (filtertype == "week")
                    WeekClicked(new object(), new EventArgs());
                else if (filtertype == "day")
                    DayClicked(new object(), new EventArgs());
            }

            else
            {
                Debug.WriteLine("show popup failed");
            }
            Debug.WriteLine("popupmethod method ended");
        }
        public async void press_BtnClicked(object sender,ClickedEventArgs e)
        {
            var  result = await DisplayActionSheet("Do you want to?", "Cancel", null, "Edit", "Delete");
          //  var pop = new XLabs.Forms.Controls.PopupLayout();
            if (result == "Edit")
            {
                var Celldata = (Listviewmodel)((Xamarin.Forms.Button)sender).BindingContext;
                string Title = "Edit Expense";
                string categorytype = "Expense";
                double amount = Celldata.amount;
                string categoryname = Celldata.categoryname;
                string date = Celldata.date;
                int Id = Celldata.ID;
                await Navigation.PushAsync(new IncomeandExpensePage(Title, categorytype, amount, categoryname, date, Id));
            }
            else if (result == "Delete")
            {
                var Celldatas = (Listviewmodel)((Xamarin.Forms.Button)sender).BindingContext;
                // TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();

                // obj.IncAndExp_id = Celldatas.ID;
                var results = await DisplayAlert("You Selected item", "Do you want to delete", "Yes", "No");
                if (results == true)
                {
                    await App.Database.delete(Celldatas.ID.ToString());

                }
                if (filtertype == "month")
                    MonthClicked(new object(), new EventArgs());
                else if (filtertype == "week")
                    WeekClicked(new object(), new EventArgs());
                else if (filtertype == "day")
                    DayClicked(new object(), new EventArgs());
            }
        }


        void OnTapGestureRecognizer(object sender, EventArgs args)
        {
            pickerMonth.SelectedItem = pickerMonth.Items[pickerMonth.SelectedIndex];
            if (pickerMonth.SelectedIndex == 0)
            {
                pickerMonth.SelectedItem = pickerMonth.Items[pickerMonth.SelectedIndex + 11];
                pickeryear.SelectedItem = pickeryear.Items[pickeryear.SelectedIndex - 1];
            }

            else
                pickerMonth.SelectedItem = pickerMonth.Items[pickerMonth.SelectedIndex - 1];

        }
        public void setindexvalue()
        {
            pickerMonth.SelectedItem = DateTime.Now.ToString("MMMM");
            pickeryear.SelectedItem = DateTime.Now.ToString("yyyy");
        }
        void onTapGestureRecognizer(object sender, EventArgs args)
        {

            if (pickerMonth.SelectedIndex == 11)
            {
                pickerMonth.SelectedItem = pickerMonth.Items[pickerMonth.SelectedIndex - 11];
                pickeryear.SelectedItem = pickeryear.Items[pickeryear.SelectedIndex + 1];


            }
            else
                pickerMonth.SelectedItem = pickerMonth.Items[pickerMonth.SelectedIndex + 1];
        }
        private void monthpicker_selectedindexchanged(object sender, EventArgs e)
        {
            try
            {
                if (count != 1)
                {
                    MonthClicked(sender, e);
                  
                }
                else
                    count++;
            }
            catch(Exception exc)
            {
                Debug.WriteLine(exc.Message + Environment.NewLine + exc.Message + exc.StackTrace);
            }
            
        }
        private void yearpicker_selectedindexchanged(object sender, EventArgs e)
        {
            if (count != 2)
            { 
                MonthClicked(sender, e);
               
            }
            else
                count++;
        }
        private async void MonthClicked(object sender, EventArgs e)
        {
            Application.Current.Properties["filter"] = "month";
            filtertype = "month";
            entry = 0;
            dayentry = 0;    
            count++;
            changeday.IsVisible = false;
            changeDate.IsVisible = true;
            changeweek.IsVisible = false;
            if (count == 1)
                setindexvalue();
            try
            {

                string cmth = pickerMonth.SelectedItem.ToString();
                string cyr = pickeryear.SelectedItem.ToString();




                double Fulltotal = 0;
                double subtotal = 0;
                string currentdate = DateTime.Now.ToString("MMMM");
                string type = "Expense";
                var list = await App.Database.GetincomeItemAsync(type);
                List<Listviewmodel> Monthlist = new List<Listviewmodel>();
                List<TBL_IncomeandExpenseDetails> monthlist = new List<TBL_IncomeandExpenseDetails>();

                for (int i = 0; i < list.Count; i++)
                {
                    try { 
                    DateTime date = Convert.ToDateTime(list[i].IncAndExp_date);
                    string mn = date.ToString("MMMMyyyy");
                    string crntdate = String.Concat(cmth, cyr);
                    string mth = date.ToString("MMMM");
                    if (crntdate == mn)
                    {
                        TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();
                        obj.IncAndExprecorded_date = list[i].IncAndExprecorded_date;
                        obj.IncAndExp_amount = list[i].IncAndExp_amount;
                        obj.IncAndExp_categoryid = list[i].IncAndExp_categoryid;
                        obj.IncAndExp_date = list[i].IncAndExp_date;
                        obj.IncAndExp_id = list[i].IncAndExp_id;
                        obj.IncAndExp_type = list[i].IncAndExp_type;
                        monthlist.Add(obj);
                    }
                    }
                    catch(Exception excep)
                    {
                        Debug.WriteLine(excep.Message + Environment.NewLine + excep.Message + excep.StackTrace);
                    }
                }

                for (int i = 0; i < monthlist.Count; i++)
                {
                    Fulltotal += monthlist[i].IncAndExp_amount;
                    Amuntlbl.Text = Fulltotal.ToString();

                }


                for (int i = 0; i < monthlist.Count; i++)
                {
                    try
                    {

                    
                        List<int> categorytbl = new List<int>();

                        DateTime date = Convert.ToDateTime(monthlist[i].IncAndExp_date);
                        string month = date.ToString("MMMM");
                        string mnth = date.ToString("MMMMyyyy");
                        string currentdatestr = String.Concat(cmth, cyr);
                        if (currentdatestr == mnth)
                        {
                            try
                            {

                                int id = monthlist[i].IncAndExp_categoryid;
                                var iddetails = await App.Database.getCategoryItemsAsync();
                                foreach (TBL_category category in iddetails)
                                {
                                    int ID = category.category_id;

                                    if (ID == id)
                                    {

                                        Listviewmodel obj = new Listviewmodel();
                                        string Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                                        obj.date = monthlist[i].IncAndExp_date;
                                        obj.amount = monthlist[i].IncAndExp_amount;
                                        obj.categoryname = Name;
                                        obj.ID = monthlist[i].IncAndExp_id;
                                        if (i == 0)
                                        {
                                            obj.total = Fulltotal;

                                        }
                                        else
                                        {
                                            subtotal += monthlist[i - 1].IncAndExp_amount;
                                            obj.total = Fulltotal - subtotal;

                                        }

                                        Monthlist.Add(obj);

                                    }
                                }
                            }
                            catch(Exception exxx)
                            {
                                Debug.WriteLine(exxx.Message + Environment.NewLine + exxx.Message + exxx.StackTrace);
                            }

                        }

                        else
                            continue;
                    }
                    catch(Exception exx)
                    {
                        Debug.WriteLine(exx.Message + Environment.NewLine + exx.Message + exx.StackTrace);
                    }
                    }
                    if (Monthlist.Count != 0)
                    {
                        Emptylist.IsVisible = false;

                        Expenselist.ItemsSource = Monthlist;
                        Expenselist.IsVisible = true;
                    }
                    else
                    {

                        IsToShowPopup(true);
                    }
                    void IsToShowPopup(bool IsToShow)
                    {
                        Expenselist.IsVisible = false;
                        Emptylist.IsVisible = true;
                    }
                
             
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message + Environment.NewLine + ex.Message);
            }
        }


        void OnweeklessTapGesture(object sender, EventArgs args)
        {
            if (weekpicker.SelectedIndex == 0)
            {

                if (mnthpicker.SelectedIndex == 0)
                {
                    yrpicker.SelectedItem = yrpicker.Items[yrpicker.SelectedIndex - 1];
                    mnthpicker.SelectedItem = mnthpicker.Items[mnthpicker.SelectedIndex = 11];
                    weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 4];
                }
                else
                {
                    mnthpicker.SelectedItem = mnthpicker.Items[mnthpicker.SelectedIndex - 1];
                    if (mnthpicker.SelectedIndex == 2 || mnthpicker.SelectedIndex == 5 || mnthpicker.SelectedIndex == 8 || mnthpicker.SelectedIndex == 11)
                    {
                        weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 4];
                    }
                    else
                    {
                        weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 3];
                    }
                }
            }
            else
                weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex - 1];

        }
        void OnweekincreaseTapGesture(object sender, EventArgs args)
        {
            if (mnthpicker.SelectedIndex == 2 || mnthpicker.SelectedIndex == 5 || mnthpicker.SelectedIndex == 8 || mnthpicker.SelectedIndex == 11)
            {
                if (weekpicker.SelectedIndex == 4)
                {

                    if (mnthpicker.SelectedIndex == 11)
                    {
                        yrpicker.SelectedItem = yrpicker.Items[yrpicker.SelectedIndex + 1];
                        mnthpicker.SelectedItem = mnthpicker.Items[mnthpicker.SelectedIndex = 0];
                        weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 0];
                    }
                    else
                        mnthpicker.SelectedItem = mnthpicker.Items[mnthpicker.SelectedIndex + 1];
                    weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 0];
                }
                else
                {
                    weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex + 1];
                }

            }
            else if (weekpicker.SelectedIndex == 3)
            {
                if (mnthpicker.SelectedIndex == 11)
                {
                    yrpicker.SelectedItem = yrpicker.Items[yrpicker.SelectedIndex + 1];
                    mnthpicker.SelectedItem = mnthpicker.Items[mnthpicker.SelectedIndex = 0];
                    weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 0];
                }
                else
                {
                    mnthpicker.SelectedItem = mnthpicker.Items[mnthpicker.SelectedIndex + 1];
                    weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex = 0];

                }
            }
            else
                weekpicker.SelectedItem = weekpicker.Items[weekpicker.SelectedIndex + 1];
        }
        private void mnthpicker_selectedindexchanged(object sender, EventArgs e)
        {
            if (entry != 1)
                WeekClicked(sender, e);
            else
                entry++;
        }
        private void weekpicker_selectedindexchanged(object sender, EventArgs e)
        {
            if(weekpicker.SelectedItem!=null)
              Getdataperweek();
        }
        private void yrpicker_selectedindexchanged(object sender, EventArgs e)
        {
            if (entry != 2)
                WeekClicked(sender, e);
            else
                entry++;
        }
        public void setselecteditem()
        {
            mnthpicker.SelectedItem = DateTime.Now.ToString("MMMM");
            yrpicker.SelectedItem = DateTime.Now.ToString("yyyy");
            var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek,
            DayOfWeek.Sunday) -
        CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now.AddDays(1 - DateTime.Now.Day),
            CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday) + 1;
            DateTime givenDate = DateTime.Now;







            DateTime StartDate = givenDate.AddDays(-((int)givenDate.DayOfWeek));
            DateTime EndDate = StartDate.AddDays(7).AddSeconds(-1);
            if (weekNumber == 1)
            {
                string week1 = string.Concat("Week1(" + StartDate.ToString("MMM,dd") + "-" + EndDate.ToString("MMM,dd") + ")");
                weekpicker.SelectedItem = week1;

            }
            else if (weekNumber == 2)
            {
                string week2 = string.Concat("Week2(" + StartDate.ToString("MMM,dd") + "-" + EndDate.ToString("MMM,dd") + ")");
                weekpicker.SelectedItem = week2;
            }
            else if (weekNumber == 3)
            {
                string week3 = string.Concat("Week3(" + StartDate.ToString("MMM,dd") + "-" + EndDate.ToString("MMM,dd") + ")");
                weekpicker.SelectedItem = week3;
            }
            else if (weekNumber == 4)
            {
                string week4 = string.Concat("Week4(" + StartDate.ToString("MMM,dd") + "-" + EndDate.ToString("MMM,dd") + ")");
                weekpicker.SelectedItem = week4;
            }
            else if (weekNumber == 5)
            {
                string week5 = string.Concat("Week5(" + StartDate.ToString("MMM,dd") + "-" + EndDate.ToString("MMM,dd") + ")");
                weekpicker.SelectedItem = week5;
            }
        }
        private void WeekClicked(object sender, EventArgs e)
        {
            try
            {
                filtertype = "week";
                Application.Current.Properties["filter"] = "week";
                changeweek.IsVisible = true;
                changeDate.IsVisible = false;
                changeday.IsVisible = false;
                entry++;
                count = 0;
                dayentry = 0;
                if (entry == 1)
                {
                    setselecteditem();
                }


                int mnindex = mnthpicker.SelectedIndex + 1;
                string selectedyr = yrpicker.SelectedItem.ToString();
                //var givendate = string.Concat("01"+","+mnindex + "," + selectedyr);
                var givendate = string.Concat(selectedyr + "," + mnindex + "," + "01");
                DateTime givenDate = Convert.ToDateTime(givendate);


                List<string> weeklist = new List<string>();


                DateTime StartDate = givenDate.AddDays(-((int)givenDate.DayOfWeek));

                DateTime EndDate = StartDate.AddDays(7).AddSeconds(-1);


                string week1 = string.Concat("Week1(" + StartDate.ToString("MMM,dd") + "-" + EndDate.ToString("MMM,dd") + ")");
                weeklist.Add(week1);
                string week2 = string.Concat("Week2(" + EndDate.AddDays(1).ToString("MMM,dd") + "-" + EndDate.AddDays(7).ToString("MMM,dd") + ")");
                weeklist.Add(week2);
                string week3 = string.Concat("Week3(" + EndDate.AddDays(8).ToString("MMM,dd") + "-" + EndDate.AddDays(14).ToString("MMM,dd") + ")");
                weeklist.Add(week3);
                string week4 = string.Concat("Week4(" + EndDate.AddDays(15).ToString("MMM,dd") + "-" + EndDate.AddDays(21).ToString("MMM,dd") + ")");
                weeklist.Add(week4);

                if (mnindex == 3 || mnindex == 6 || mnindex == 9 || mnindex == 12)
                {
                    string week5 = string.Concat("Week5(" + EndDate.AddDays(22).ToString("MMM,dd") + "-" + EndDate.AddDays(28).ToString("MMM,dd") + ")");
                    weeklist.Add(week5);
                }


                weekpicker.ItemsSource = weeklist;

                var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek,
              DayOfWeek.Sunday) -
              CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now.AddDays(1 - DateTime.Now.Day),
              CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday) + 1;
                if (weekNumber == 1)
                {
                    weekpicker.SelectedItem = week1;
                }
                else if (weekNumber == 2)
                {
                    weekpicker.SelectedItem = week2;
                }
                else if (weekNumber == 3)
                {
                    weekpicker.SelectedItem = week3;
                }
                else if (weekNumber == 4)
                {
                    weekpicker.SelectedItem = week4;
                }
                else if (weekNumber == 5)
                {
                    string week5 = string.Concat("Week5(" + EndDate.AddDays(22).ToString("MMM,dd") + "-" + EndDate.AddDays(28).ToString("MMM,dd") + ")");
                    weekpicker.SelectedItem = week5;
                }


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message + Environment.NewLine + ex.Message);
            }
        }
        public async void Getdataperweek()
        {
            try
            {
                double Fulltotal = 0;
                double subtotal = 0;
                Amuntlbl.Text = "0";
                string weekdate = weekpicker.SelectedItem.ToString();
                string currentyr = yrpicker.SelectedItem.ToString();
                string strOutput = weekdate.Split(new char[] { '(', ')' })[1];
                string[] tokens = strOutput.Split('-');
                string concatfstdate = string.Concat(tokens[0] + "," + currentyr);
                string concatlstdate = string.Concat(tokens[1] + "," + currentyr);
                DateTime fstdate = Convert.ToDateTime(concatfstdate);
                DateTime lsdate = Convert.ToDateTime(concatlstdate);


                string type = "Expense";
                var list = await App.Database.GetincomeItemAsync(type);
                List<Listviewmodel> Weeklist = new List<Listviewmodel>();
                List<TBL_IncomeandExpenseDetails> weeklist = new List<TBL_IncomeandExpenseDetails>();

                for (int i = 0; i < list.Count; i++)
                {
                    DateTime dt = Convert.ToDateTime(list[i].IncAndExp_date);
                    var dat = dt;
                    if (fstdate <= dat && dat <= lsdate)
                    {
                        TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();
                        obj.IncAndExprecorded_date = list[i].IncAndExprecorded_date;
                        obj.IncAndExp_amount = list[i].IncAndExp_amount;
                        obj.IncAndExp_categoryid = list[i].IncAndExp_categoryid;
                        obj.IncAndExp_date = list[i].IncAndExp_date;
                        obj.IncAndExp_id = list[i].IncAndExp_id;
                        obj.IncAndExp_type = list[i].IncAndExp_type;
                        weeklist.Add(obj);
                    }
                }



                for (int i = 0; i < weeklist.Count; i++)
                {
                    DateTime dt = Convert.ToDateTime(weeklist[i].IncAndExp_date);
                    var dat = dt;
                    if (fstdate <= dat && dat <= lsdate)
                    {
                        Fulltotal += weeklist[i].IncAndExp_amount;
                        Amuntlbl.Text = Fulltotal.ToString();
                    }

                }
                for (int i = 0; i < weeklist.Count; i++)
                {
                    List<int> categorytbl = new List<int>();

                    DateTime dt = Convert.ToDateTime(weeklist[i].IncAndExp_date);
                    var dat = dt;
                    if (fstdate <= dat && dat <= lsdate)
                    {

                        int id = weeklist[i].IncAndExp_categoryid;
                        var iddetails = await App.Database.getCategoryItemsAsync();
                        foreach (TBL_category category in iddetails)
                        {
                            int ID = category.category_id;

                            if (ID == id)
                            {
                                Listviewmodel obj = new Listviewmodel();
                                string Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                                obj.date = weeklist[i].IncAndExp_date;
                                obj.amount = weeklist[i].IncAndExp_amount;
                                obj.categoryname = Name;
                                obj.ID = weeklist[i].IncAndExp_id;
                                if (i == 0)
                                    obj.total = Fulltotal;
                                else
                                {
                                    subtotal += weeklist[i - 1].IncAndExp_amount;
                                    obj.total = Fulltotal - subtotal;
                                }
                                Weeklist.Add(obj);

                            }
                        }
                    }

                    else
                        continue;
                }

                if(weeklist.Count!=-0)
                {
                    Emptylist.IsVisible = false;
                    Expenselist.IsVisible = true;
                    Expenselist.ItemsSource = Weeklist.AsEnumerable();
                }
               else
                {
                    Emptylist.IsVisible = true;
                    Expenselist.IsVisible = false;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message + Environment.NewLine + ex.Message);
            }


        }


        void setcurrentdate()
        {
            daymnpicker.SelectedItem = DateTime.Now.ToString("MMMM");
            dayyrpicker.SelectedItem = DateTime.Now.ToString("yyyy");
            daypicker.SelectedItem = DateTime.Now.ToString("dd");
        }
        private void daymnpicker_selectedindexchanged(object sender, EventArgs e)
        {
            if (dayentry != 1)
                DayClicked(sender, e);
            else
                dayentry++;


        }
        private void dayyrpicker_selectedindexchanged(object sender, EventArgs e)
        {
            if (dayentry != 2)
                DayClicked(sender, e);
            else
                dayentry++;
        }
        private void daypicker_selectedindexchanged(object sender, EventArgs e)
        {
            if(daypicker.SelectedItem!=null)
            Getdataperday();

        }
        void OndaylessTapGesture(object sender, EventArgs args)
        {
            try
            {


                if (daypicker.SelectedIndex == 0)
                {
                    if (daymnpicker.SelectedIndex == 0)
                    {
                        dayyrpicker.SelectedItem = dayyrpicker.Items[dayyrpicker.SelectedIndex - 1];
                        daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex = 11];
                        daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 30];
                    }

                    else
                    {
                        daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex - 1];
                        if (daymnpicker.SelectedIndex == 1)
                        {
                            string cryear = dayyrpicker.SelectedItem.ToString();
                            int cuyear = Convert.ToInt16(cryear);


                            if ((cuyear % 4 == 0 && cuyear % 100 != 0) || (cuyear % 400 == 0))
                            {
                                daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 28];
                            }
                            else
                            {
                                daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 27];
                            }
                        }
                        else if (daymnpicker.SelectedIndex % 2 == 0)
                        {
                            daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 30];

                        }
                        else if (daymnpicker.SelectedIndex % 2 != 0)
                        {
                            daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 29];
                        }

                    }
                }
                else
                {
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex - 1];

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message + Environment.NewLine + ex.Message);
            }
        }
        void OndayincTapGesture(object sender, EventArgs args)
        {
            if (daypicker.SelectedIndex == 27)
            {

                if (daymnpicker.SelectedIndex == 1)
                {
                    string cryear = dayyrpicker.SelectedItem.ToString();
                    int cuyear = Convert.ToInt16(cryear);

                    if ((cuyear % 4 == 0 && cuyear % 100 != 0) || (cuyear % 400 == 0))
                    {
                        daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex + 1];
                    }
                    else
                    {


                        daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex + 1];
                        daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 0];

                    }
                }
                else
                {
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex + 1];
                }

            }
            else if (daypicker.SelectedIndex == 28)
            {
                if (daymnpicker.SelectedIndex == 1)
                {
                    daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex + 1];
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 0];
                }
                else
                {
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex + 1];
                }
            }
            else if (daypicker.SelectedIndex == 29)
            {
                if (daymnpicker.SelectedIndex == 10 || daymnpicker.SelectedIndex == 8)
                {
                    daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex + 1];
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 0];
                }
                else if (daymnpicker.SelectedIndex == 7 || daymnpicker.SelectedIndex == 9 || daymnpicker.SelectedIndex == 11 || daymnpicker.SelectedIndex % 2 == 0)
                {
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex + 1];
                }

            }
            /* else if(daypicker.SelectedIndex==29)
             {
                 if((daymnpicker.SelectedIndex != 10 || daymnpicker.SelectedIndex != 8 || daymnpicker.SelectedIndex==7|| daymnpicker.SelectedIndex == 9|| daymnpicker.SelectedIndex == 11|| daymnpicker.SelectedIndex % 2 == 0 ))
                 {
                     daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex + 1];
                 }
                 else
                 {
                     daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex + 1];
                     daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 0];
                 }
             }*/
            else if (daypicker.SelectedIndex == 30)
            {
                if (daymnpicker.SelectedIndex == 11)
                {
                    dayyrpicker.SelectedItem = dayyrpicker.Items[dayyrpicker.SelectedIndex + 1];
                    daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex = 0];
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 0];
                }
                else
                {
                    daymnpicker.SelectedItem = daymnpicker.Items[daymnpicker.SelectedIndex + 1];
                    daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex = 0];

                }
            }
            else
                daypicker.SelectedItem = daypicker.Items[daypicker.SelectedIndex + 1];
        }
        public async void Getdataperday()
        {
            try
            {


                string dateforday = string.Concat(daymnpicker.SelectedItem.ToString() + "," + daypicker.SelectedItem.ToString() + "," + dayyrpicker.SelectedItem.ToString());
                DateTime day = Convert.ToDateTime(dateforday);
                double Fulltotal = 0;
                double subtotal = 0;
                Amuntlbl.Text = "0";
                string type = "Expense";
                var list = await App.Database.GetincomeItemAsync(type);
                List<Listviewmodel> Daylist = new List<Listviewmodel>();
                List<TBL_IncomeandExpenseDetails> daylist = new List<TBL_IncomeandExpenseDetails>();

                for (int i = 0; i < list.Count; i++)
                {
                    DateTime dt = Convert.ToDateTime(list[i].IncAndExp_date);
                    var dat = dt;
                    if (dt == day)
                    {
                        TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();
                        obj.IncAndExprecorded_date = list[i].IncAndExprecorded_date;
                        obj.IncAndExp_amount = list[i].IncAndExp_amount;
                        obj.IncAndExp_categoryid = list[i].IncAndExp_categoryid;
                        obj.IncAndExp_date = list[i].IncAndExp_date;
                        obj.IncAndExp_id = list[i].IncAndExp_id;
                        obj.IncAndExp_type = list[i].IncAndExp_type;
                        daylist.Add(obj);
                    }
                }
                for (int i = 0; i < daylist.Count; i++)
                {
                    DateTime dt = Convert.ToDateTime(daylist[i].IncAndExp_date);
                    var dat = dt;
                    if (dt == day)
                    {
                        Fulltotal += daylist[i].IncAndExp_amount;
                        Amuntlbl.Text = Fulltotal.ToString();
                    }
                }

                for (int i = 0; i < daylist.Count; i++)
                {
                    List<int> categorytbl = new List<int>();

                    DateTime dt = Convert.ToDateTime(daylist[i].IncAndExp_date);
                    var dat = dt;
                    if (dt == day)
                    {


                        int id = daylist[i].IncAndExp_categoryid;
                        var iddetails = await App.Database.getCategoryItemsAsync();
                        foreach (TBL_category category in iddetails)
                        {
                            int ID = category.category_id;

                            if (ID == id)
                            {
                                Listviewmodel obj = new Listviewmodel();
                                string Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                                obj.date = daylist[i].IncAndExp_date;
                                obj.amount = daylist[i].IncAndExp_amount;
                                obj.categoryname = Name;
                                obj.ID = daylist[i].IncAndExp_id;
                                if (i == 0)
                                    obj.total = Fulltotal;
                                else
                                {
                                    subtotal += daylist[i - 1].IncAndExp_amount;
                                    obj.total = Fulltotal - subtotal;
                                }
                                Daylist.Add(obj);

                            }
                        }
                    }

                    else
                        continue;


                }

                if(Daylist.Count!=0)
                {
                    Emptylist.IsVisible = false;
                    Expenselist.IsVisible = true;
                    Expenselist.ItemsSource = Daylist;
                }
                else
                {
                    Emptylist.IsVisible = true;
                    Expenselist.IsVisible = false;

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message + Environment.NewLine + ex.Message);
            }
        }
        private  void DayClicked(object sender, EventArgs e)
        {
            filtertype = "day";
            Application.Current.Properties["filter"] = "day";
            changeday.IsVisible = true;
            changeDate.IsVisible = false;
            changeweek.IsVisible = false;
            dayentry++;
            entry = 0;
            count = 0;
            if (dayentry == 1)
            {
                setcurrentdate();
            }
            string cryear = dayyrpicker.SelectedItem.ToString();
            int cuyear = Convert.ToInt16(cryear);

            if (daymnpicker.SelectedIndex == 1)
            {
                if ((cuyear % 4 == 0 && cuyear % 100 != 0) || (cuyear % 400 == 0))
                {
                    string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29" };
                    List<string> nodays = new List<string>();
                    for (int i = 0; i < days.Count(); i++)
                    {
                        nodays.AddRange(days);
                        daypicker.ItemsSource = nodays;

                    }
                    daypicker.SelectedItem = nodays[0];
                }
                else
                {
                    string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28" };
                    List<string> nodays = new List<string>();
                    for (int i = 0; i < days.Count(); i++)
                    {
                        nodays.AddRange(days);
                        daypicker.ItemsSource = nodays;
                    }
                    daypicker.SelectedItem = nodays[0];
                }
            }
            else if (daymnpicker.SelectedIndex == 7)
            {
                string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
                List<string> nodays = new List<string>();
                for (int i = 0; i < days.Count(); i++)
                {
                    nodays.AddRange(days);
                    daypicker.ItemsSource = nodays;
                }
                daypicker.SelectedItem = nodays[0];
            }
            else if (daymnpicker.SelectedIndex == 8 || daymnpicker.SelectedIndex == 10)
            {
                string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
                List<string> nodays = new List<string>();
                for (int i = 0; i < days.Count(); i++)
                {
                    nodays.AddRange(days);
                    daypicker.ItemsSource = nodays;
                }
                daypicker.SelectedItem = nodays[0];
            }
            else if (daymnpicker.SelectedIndex == 9 || daymnpicker.SelectedIndex == 11)
            {
                string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
                List<string> nodays = new List<string>();
                for (int i = 0; i < days.Count(); i++)
                {
                    nodays.AddRange(days);
                    daypicker.ItemsSource = nodays;
                }
                daypicker.SelectedItem = nodays[0];
            }

            else
            {
                if (daymnpicker.SelectedIndex % 2 == 0)
                {
                    string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
                    List<string> nodays = new List<string>();
                    for (int i = 0; i < days.Count(); i++)
                    {
                        nodays.AddRange(days);
                        daypicker.ItemsSource = nodays;
                    }
                    daypicker.SelectedItem = nodays[0];

                }
                else
                {
                    string[] days = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
                    List<string> nodays = new List<string>();
                    for (int i = 0; i < days.Count(); i++)
                    {
                        nodays.AddRange(days);
                        daypicker.ItemsSource = nodays;
                    }
                    daypicker.SelectedItem = nodays[0];
                }

            }
            string date = daymnpicker.SelectedItem.ToString();
                if(date==DateTime.Now.ToString("MMMM"))
                    daypicker.SelectedItem = DateTime.Now.ToString("dd");

           
        }







        public async void ExpenseList()
        {
            double totalamount = 0;
            string type = "Expense";
            var list = await App.Database.GetincomeItemAsync(type);
           double Fulltotal = 0;
            double subtotal = 0;
            Debug.WriteLine("In ExpenseList, list retrived count  => " + list.Count);


            List<string> categorynamelist = new List<string>();
            List<Listviewmodel> expenselist = new List<Listviewmodel>();
           
            string Name;

            foreach(TBL_IncomeandExpenseDetails totallist in list)
            {
                Fulltotal += totallist.IncAndExp_amount;
                Amuntlbl.Text = Fulltotal.ToString();
            }
              
            for(int i=0;i<list.Count;i++)
            {
                List<int> categorytbl = new List<int>();
                int id = list[i].IncAndExp_categoryid;
                var iddetails = await App.Database.getCategoryItemsAsync();
                foreach (TBL_category category in iddetails)
                {
                    int ID = category.category_id;

                    if (ID == id)
                    {
                        Listviewmodel obj = new Listviewmodel();

                        Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                        obj.date = list[i].IncAndExp_date;
                        obj.amount = list[i].IncAndExp_amount;
                        if (i == 0)
                            obj.total = Fulltotal;
                        else
                        {
                            subtotal += list[i - 1].IncAndExp_amount;
                            obj.total = Fulltotal - subtotal;
                        }
                        obj.categoryname = Name;
                      

                        expenselist.Add(obj);

                        Debug.WriteLine("In ExpenseList, expenslist count=>" + expenselist.Count);
                    }
                }




            }


            if (expenselist.Count != 0)
            {
                Emptylist.IsVisible = false;
                Expenselist.ItemsSource = expenselist;
               // amountlbl.IsVisible = true;
              //  Amountlbl.IsVisible = true;
                Debug.WriteLine("In ExpenseList, if case-added reverse list to Expenselist.ItemsSource");
                Expenselist.IsVisible = true;
            }
            else
            {
                Debug.WriteLine("in ExpenseList ,else in adding ExpenseList");
                IsToShowPopup(true);
            }
            Debug.WriteLine("End of calling ExpenseList");
        }
        void IsToShowPopup(bool IsToShow)
        {
            Expenselist.IsVisible = false;
            Emptylist.IsVisible = true;
        }
       
        /*   private async void MonthClicked(object sender, EventArgs e)
        {
            double total = 0;
            string currentdate = DateTime.Now.ToString("MMMM");
            string type = "Expense";
            double Fulltotal = 0;
            double subtotal = 0;
            var list = await App.Database.GetincomeItemAsync(type);
            List<Listviewmodel> Monthlist = new List<Listviewmodel>();
            List<TBL_IncomeandExpenseDetails> monthlist = new List<TBL_IncomeandExpenseDetails>();

            for (int i = 0; i < list.Count; i++)
            {
                DateTime date = Convert.ToDateTime(list[i].IncAndExp_date);
                string month = date.ToString("MMMM");
                if (currentdate == month)
                { 
                    TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();
                    obj.IncAndExprecorded_date = list[i].IncAndExprecorded_date;
                    obj.IncAndExp_amount = list[i].IncAndExp_amount;
                    obj.IncAndExp_categoryid = list[i].IncAndExp_categoryid;
                    obj.IncAndExp_date = list[i].IncAndExp_date;
                    obj.IncAndExp_id = list[i].IncAndExp_id;
                    obj.IncAndExp_type = list[i].IncAndExp_type;
                    monthlist.Add(obj);
                }
            }


            for (int i=0;i<monthlist.Count;i++)
            {
                DateTime date = Convert.ToDateTime(monthlist[i].IncAndExp_date);
                string month = date.ToString("MMMM");
                if (currentdate == month)
                    Fulltotal += monthlist[i].IncAndExp_amount;

                else
                    continue;
                Amountlbl.Text = Fulltotal.ToString();
            }
                

            for (int i=0;i<monthlist.Count;i++)
            {
                List<int> categorytbl = new List<int>();

                DateTime date = Convert.ToDateTime(monthlist[i].IncAndExp_date);
                string month = date.ToString("MMMM");
                if (currentdate == month)
                {
                   // total = total + lst.IncAndExp_amount;
                    int id = monthlist[i].IncAndExp_categoryid;
                    var iddetails = await App.Database.getCategoryItemsAsync();
                    foreach (TBL_category category in iddetails)
                    {
                        int ID = category.category_id;

                        if (ID == id)
                        {
                            Listviewmodel obj = new Listviewmodel();
                            string Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                            obj.date = monthlist[i].IncAndExp_date;
                            obj.amount = monthlist[i].IncAndExp_amount;
                            obj.categoryname = Name;
                            if(i==0)
                               obj.total = Fulltotal;
                            else
                            {
                                subtotal += monthlist[i - 1].IncAndExp_amount;
                                obj.total = Fulltotal - subtotal;
                            }
                            Monthlist.Add(obj);

                        }
                    }
                }

                else
                    continue;



            }





        

            Expenselist.ItemsSource = Monthlist.AsEnumerable();
        }*/
        /* private async void WeekClicked(object sender, EventArgs e)
        {
           
            double Fulltotal = 0;
            double subtotal = 0;
            var givenDate = DateTime.Now;
            var intervalToStart = givenDate.DayOfWeek - DayOfWeek.Monday;
            var startDate = givenDate.AddDays(-intervalToStart);

            DateTime endDate;
            int intervalToEnd = 0;

            if (startDate.Month != givenDate.Month)
                startDate = new DateTime(givenDate.Year, givenDate.Month, 1);

            var dayOfWeekStartDate = startDate.DayOfWeek.ToString().ToLower();

            switch (dayOfWeekStartDate)
            {
                case "sunday":
                    intervalToEnd = 0;
                    break;
                case "monday":
                    intervalToEnd = 6;
                    break;
                case "tuesday":
                    intervalToEnd = 5;
                    break;
                case "wednesday":
                    intervalToEnd = 4;
                    break;
                case "thursday":
                    intervalToEnd = 3;
                    break;
                case "friday":
                    intervalToEnd = 2;
                    break;
                case "saturday":
                    intervalToEnd = 1;
                    break;
            }

            endDate = startDate.AddDays(intervalToEnd);

            var date1 = startDate.ToString("MM/dd/yyyy");
          //  DateTime dt1 = Convert.ToDateTime(date1);
            DateTime dt1 = DateTime.ParseExact(date1, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            var date2 = endDate.ToString("MM/dd/yyyy");
           // DateTime dt2 = Convert.ToDateTime(date2);
            DateTime dt2 = DateTime.ParseExact(date2, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            string type = "Expense";
            var list = await App.Database.GetincomeItemAsync(type);
            List<Listviewmodel> Weeklist = new List<Listviewmodel>();
            List<TBL_IncomeandExpenseDetails> weeklist = new List<TBL_IncomeandExpenseDetails>();

            for (int i = 0; i < list.Count; i++)
            {
                DateTime dt = Convert.ToDateTime(list[i].IncAndExp_date);
                var dat = dt;
                if (dt1 <= dat && dat <= dt2)
                {
                    TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();
                    obj.IncAndExprecorded_date = list[i].IncAndExprecorded_date;
                    obj.IncAndExp_amount = list[i].IncAndExp_amount;
                    obj.IncAndExp_categoryid = list[i].IncAndExp_categoryid;
                    obj.IncAndExp_date = list[i].IncAndExp_date;
                    obj.IncAndExp_id = list[i].IncAndExp_id;
                    obj.IncAndExp_type = list[i].IncAndExp_type;
                    weeklist.Add(obj);
                }
            }



            for (int i = 0; i < weeklist.Count; i++)
            {
                DateTime dt = Convert.ToDateTime(weeklist[i].IncAndExp_date);
                var dat = dt;
                if (dt1 <= dat && dat <= dt2)
                {
                    Fulltotal += weeklist[i].IncAndExp_amount;
                    Amountlbl.Text = Fulltotal.ToString();
                }

            }
            for(int i=0;i<weeklist.Count;i++)
            {
                List<int> categorytbl = new List<int>();

                DateTime dt = Convert.ToDateTime(weeklist[i].IncAndExp_date);
                var dat = dt;
                if (dt1 <= dat && dat <= dt2)
                {
                   // total = total + lst.IncAndExp_amount; ;
                    int id = weeklist[i].IncAndExp_categoryid;
                    var iddetails = await App.Database.getCategoryItemsAsync();
                    foreach (TBL_category category in iddetails)
                    {
                        int ID = category.category_id;

                        if (ID == id)
                        {
                            Listviewmodel obj = new Listviewmodel();
                            string Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                            obj.date = weeklist[i].IncAndExp_date;
                            obj.amount = weeklist[i].IncAndExp_amount;
                            obj.categoryname = Name;
                            if (i == 0)
                                obj.total = Fulltotal;
                            else
                            {
                                subtotal += weeklist[i-1].IncAndExp_amount;
                                obj.total = Fulltotal - subtotal;
                            }
                            Weeklist.Add(obj);

                        }
                    }
                }

                else
                    continue;
            }

          
            Expenselist.ItemsSource = Weeklist.AsEnumerable();
        }*/
        /*  private async void DayClicked(object sender, EventArgs e)
        {
          
            double Fulltotal = 0;
            double subtotal = 0;
            DateTime todaydate = DateTime.Now;
            var date1 = todaydate.ToString("MM/dd/yyyy");
            //DateTime todaydate1 = Convert.ToDateTime(date1);
            DateTime todaydate1 = DateTime.ParseExact(date1, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            string type = "Expense";
            var list = await App.Database.GetincomeItemAsync(type);
            List<Listviewmodel> Daylist = new List<Listviewmodel>();
            List<TBL_IncomeandExpenseDetails> daylist = new List<TBL_IncomeandExpenseDetails>();

            for (int i=0;i<list.Count;i++)
            {
                DateTime dt = Convert.ToDateTime(list[i].IncAndExp_date);
                var dat = dt;
                if (dt == todaydate1)
                {
                    TBL_IncomeandExpenseDetails obj = new TBL_IncomeandExpenseDetails();
                    obj.IncAndExprecorded_date = list[i].IncAndExprecorded_date;
                    obj.IncAndExp_amount = list[i].IncAndExp_amount;
                    obj.IncAndExp_categoryid = list[i].IncAndExp_categoryid;
                    obj.IncAndExp_date = list[i].IncAndExp_date;
                    obj.IncAndExp_id = list[i].IncAndExp_id;
                    obj.IncAndExp_type = list[i].IncAndExp_type;
                    daylist.Add(obj);
                }
            }
            for (int i = 0; i < daylist.Count; i++)
            {
                DateTime dt = Convert.ToDateTime(daylist[i].IncAndExp_date);
                var dat = dt;
                if (dt == todaydate1)
                {
                    Fulltotal += daylist[i].IncAndExp_amount;
                    Amountlbl.Text = Fulltotal.ToString(); 
                }
            }


            for(int i=0;i<daylist.Count;i++)
            {
                List<int> categorytbl = new List<int>();

                DateTime dt = Convert.ToDateTime(daylist[i].IncAndExp_date);
                var dat = dt;
                if (dt == todaydate1)
                {
                  //  total = total + lst.IncAndExp_amount;

                    int id = daylist[i].IncAndExp_categoryid;
                    var iddetails = await App.Database.getCategoryItemsAsync();
                    foreach (TBL_category category in iddetails)
                    {
                        int ID = category.category_id;

                        if (ID == id)
                        {
                            Listviewmodel obj = new Listviewmodel();
                            string Name = category.category_name.First().ToString().ToUpper() + category.category_name.Substring(1);
                            obj.date = daylist[i].IncAndExp_date;
                            obj.amount = daylist[i].IncAndExp_amount;
                            obj.categoryname = Name;
                            if(i==0)
                            obj.total = Fulltotal;
                            else
                            {
                                subtotal += daylist[i - 1].IncAndExp_amount;
                                obj.total = Fulltotal - subtotal;
                            }
                            Daylist.Add(obj);

                        }
                    }
                }

                else
                    continue;
            }


         
            Expenselist.ItemsSource = Daylist.AsEnumerable();


        }*/
        /*  private async void AllClicked(object sender, EventArgs e)
        {
            ExpenseList();
        }*/
        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            refreshData();

        }
        public void  refreshData()
        {
            if (filtertype == "month")
                MonthClicked(new object(), new EventArgs());
            else if (filtertype == "week")
                WeekClicked(new object(), new EventArgs());
            else if (filtertype == "day")
                DayClicked(new object(), new EventArgs());

        }

        
    }
}

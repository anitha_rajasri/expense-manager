﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyManagement
{
   public interface IToastMessage
    {
        void DisplayMessage(string message); 
    }
}

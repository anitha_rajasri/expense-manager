﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MoneyManagement
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Demo : ContentPage
	{
		public Demo ()
		{
			InitializeComponent();
            incomelist();

        }

       

        public async void incomelist()
        {
            double total = 0;
            string type = "Income";
            var list = await App.Database.GetincomeItemAsync(type);
            List<string> categorynamelist = new List<string>();
            List<Listviewmodel> incomelist = new List<Listviewmodel>();
            string Name;
            foreach (TBL_IncomeandExpenseDetails inclist in list)
            {




                List<int> categorytbl = new List<int>();
                int id = inclist.IncAndExp_categoryid;
                var iddetails = await App.Database.getCategoryItemsAsync();
                foreach (TBL_category category in iddetails)
                {
                    categorytbl.Add(category.category_id);
                    if (categorytbl.Contains(id))
                    {
                        total = total + inclist.IncAndExp_amount;
                        Listviewmodel obj = new Listviewmodel();

                        Name = category.category_name;
                        obj.date = inclist.IncAndExp_date;
                        obj.amount = inclist.IncAndExp_amount;
                        obj.categoryname = Name;
                        obj.total = total;
                        categorytbl.Clear();
                        incomelist.Add(obj);
                       
                    }
                }
            }

            var layout = new StackLayout
            {
                Padding = new Thickness(20),
            };

            var mainContent = new ScrollView
            {
                Orientation = ScrollOrientation.Vertical,
                Padding = new Thickness(20),
                WidthRequest = 30,
                HeightRequest = 400,
                Content = layout,
                BackgroundColor = Color.Green,
            };
            var stacks = new List<StackLayout>();
            foreach (var lst in incomelist)
            {
                var stack = new StackLayout { Spacing = 0 };

                stack.Children.Add(new Label { Text = lst.amount.ToString(), Font = Font.SystemFontOfSize(30),BackgroundColor=Color.Red });

               

                stacks.Add(stack);





            }
            var mainStack = new StackLayout();
            foreach (var stack in stacks)
                mainStack.Children.Add(stack);

            layout.Children.Add(mainStack);


        }



    }
    }
/*   Content = new ScrollView()
                  {

                      Content = new StackLayout
                      {
                         Children =

                         {  new Label { Text = lst.amount.ToString(), Font = Font.SystemFontOfSize(30) },
                      }*/

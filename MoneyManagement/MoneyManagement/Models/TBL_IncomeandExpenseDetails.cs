﻿using SQLite;
namespace MoneyManagement
{
    public class TBL_IncomeandExpenseDetails
    {
        [PrimaryKey, AutoIncrement]   //Attributes
        public int IncAndExp_id { get; set; }
        public double IncAndExp_amount { get; set; }
        public string IncAndExp_date { get; set; }
        public int IncAndExp_categoryid { get; set; }
        public string IncAndExp_type { get; set; }
        public string IncAndExprecorded_date{get; set;}

    }
}

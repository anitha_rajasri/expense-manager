﻿using SQLite;
namespace MoneyManagement
{
    public class TBL_user
    {
        [PrimaryKey, AutoIncrement]     //Attributes
        public int login_id { get; set; }
        public string login_Security { get; set; }
        public string login_password { get; set; }
        public string Created_date { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MoneyManagement
{
   public class Settinglist
    {
        public string Settingitem { get; set; }
        public int itemid { get; set; }
    }

    public class PersonList : List<Settinglist>
    {
        public string Heading { get; set; }
        public List<Settinglist> Settingitems => this;

    }
}

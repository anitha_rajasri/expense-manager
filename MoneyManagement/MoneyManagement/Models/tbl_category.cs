﻿using SQLite;


namespace MoneyManagement
{
    public class TBL_category
    {
        [PrimaryKey, AutoIncrement]  //Attributes
        public int category_id { get; set; }
        [Unique]
        public string category_name { get; set; }
        public string category_type { get; set; }
        public string categoryrecorded_date { get; set; }

    }
}

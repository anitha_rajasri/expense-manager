﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyManagement
{
    public class Listviewmodel
    {
        public string date { get; set; }
        public string categoryname { get; set; }
        public double amount { get; set; }
        public double total { get; set; }
        public int ID { get; set; }
    }
}

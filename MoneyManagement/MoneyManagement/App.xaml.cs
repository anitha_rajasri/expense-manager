﻿using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MoneyManagement
{
    public partial class App : Application
    {
        static DatabaseManager database;

        string page="Home";

        public  App()
        {
            Init();
            


        }

        public async void Init()
        {
          

             var lst = await App.Database.GetpasswordAsync();
              if(lst==null)
               {
                MainPage = new NavigationPage(new HomeTabbedPage())
                {
                    BarBackgroundColor =Color.FromHex("#fc4b5f")
                    

                };
              
               }
               else if(lst.login_password==null)
               {
                MainPage = new NavigationPage(new HomeTabbedPage())
                {
                    BarBackgroundColor = Color.FromHex("#fc4b5f")

                };
            }
              else
               {

                MainPage = new LoginPage(page);

               }
            


        }

        public static DatabaseManager Database
        {
            get
            {
                if (database == null)
                {
                    database = new DatabaseManager(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "MoneyManagement.db3"));
                }
                return database;
            }
        }

         protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
          
        }
        
    
        }

    }


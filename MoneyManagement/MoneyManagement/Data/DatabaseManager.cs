﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MoneyManagement
{
    public class DatabaseManager
    {
        readonly SQLiteAsyncConnection database;
        public DatabaseManager(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<TBL_user>().Wait();
            database.CreateTableAsync<TBL_IncomeandExpenseDetails>().Wait();
            database.CreateTableAsync<TBL_category>().Wait();

        }

        public Task<List<TBL_user>> GetItemsAsync()
        {
            return database.Table<TBL_user>().ToListAsync();
        }
        public Task<List<TBL_category>> getCategoryItemsAsync()
        {
            return database.Table<TBL_category>().ToListAsync();
        }
        public Task<List<TBL_IncomeandExpenseDetails>> getIncandExpItemsAsync()
        {
            return database.Table<TBL_IncomeandExpenseDetails>().ToListAsync();
        }

        public async Task<TBL_category> GetidAsync(string name)
        {
          

            var result2 = await database.Table<TBL_category>().Where(k => k.category_name == name).FirstOrDefaultAsync();
            return result2;
        }
        public async Task<List<TBL_category>> GetnameAsync(int id)
        {
          

            var result2 = await database.Table<TBL_category>().Where(k => k.category_id == id).ToListAsync();
            return result2;
        }
        public async Task<TBL_user> GetpasswordAsync()
        {
            var result = await database.Table<TBL_user>().FirstOrDefaultAsync();
        
            return result;
        }
        public Task<TBL_user> GetItemAsync(int id)
        {
            return database.Table<TBL_user>().Where(i => i.login_id == id).FirstOrDefaultAsync();
        }
        public async Task<List<TBL_IncomeandExpenseDetails>> GetincomeItemAsync(string type)
        {
            try
            {
               return await database.Table<TBL_IncomeandExpenseDetails>().Where(j => j.IncAndExp_type == type).OrderByDescending(j => j.IncAndExp_date).ToListAsync();
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message + Environment.NewLine + e.Message + e.StackTrace);
            }

            return null;



        }
        public Task<int> SaveItemAsync(TBL_user item)
        {
            if (item.login_id != 0)
            {
                DependencyService.Get<IToastMessage>().DisplayMessage("Successfully saved");
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> SaveItemAsync(TBL_category item)
        {
           
                if (item.category_id != 0)
                {
                    return database.UpdateAsync(item);
                }
                else
                {
               // Task.Delay(2000);
                return database.InsertAsync(item);
               
                }
            
           
        }
        public async Task<int> SaveItemAsync(TBL_IncomeandExpenseDetails item)
        {
            
                if (item.IncAndExp_id != 0)
            {
                DependencyService.Get<IToastMessage>().DisplayMessage("Successfully Updated");
                return await database.UpdateAsync(item);
                }
                else
                {
                DependencyService.Get<IToastMessage>().DisplayMessage("Successfully saved");
                return await database.InsertAsync(item);
               // Task.Delay(500);  
                }
         }

        public async Task<List<TBL_category>> GetcategorylistAsync(string categorytype)
        {
            var result = await database.Table<TBL_category>().Where(j => j.category_type == categorytype).ToListAsync();
            return result;
        }

        public async Task<TBL_category> Getcategorynameperid(int  categoryid)
        {
               return await database.Table<TBL_category>().Where(i => i.category_id == categoryid).FirstOrDefaultAsync();
        }

        public async 
        Task
delete(string item)
        {
          await database.QueryAsync<TBL_IncomeandExpenseDetails>("DELETE FROM [TBL_IncomeandExpenseDetails] WHERE IncAndExp_id = " + item);
           
        }
       
      /*  public  Task<int> DeleteItemAsync(int item)
        {
           
            var data = database.Table<TBL_IncomeandExpenseDetails>().Where(s => s.IncAndExp_id == item).FirstOrDefaultAsync();
            if (data != null)
            {
                database.Delete<TBL_IncomeandExpenseDetails>(item.);
                return await database.DeleteAsync(data);
            
            }
            else
                return 0;
        }*/

    }
    
    
    }

